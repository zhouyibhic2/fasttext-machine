/*******************************************************************************
 * ATE, Automation Test Engine
 *
 * Copyright 2017, Montreal PROT, or individual contributors as
 * indicated by the @author tags or express copyright attribution
 * statements applied by the authors.  All third-party contributions are
 * distributed under license by Montreal PROT.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package com.bigtester.tebloud.machine.fasttext.service.uicollection.matcher;

import java.util.List;
import java.util.Optional;

import org.w3c.dom.Node;

// TODO: Auto-generated Javadoc
/**
 * This class IUIGrouppedByAttributeMatcher defines ....
 * @author Peidong Hu
 *
 */
public interface IUIGrouppedByAttributeMatcher extends
		IUserInputConsolidationMatcher {
	public enum SupportedMatchingInputAttribute {
		NAME("name"), ID("id");
		 private final String name;       

		    private SupportedMatchingInputAttribute(String s) {
		        name = s;
		    }

		    public boolean equalsName(String otherName) {
		        // (otherName == null) check is not needed because name.equals(null) returns false 
		        return name.equals(otherName);
		    }

		    public String toString() {
		       return this.name;
		    }
	}
	public Optional<Node> matchParentNode(List<Node> childNodes, String attrName, String attrValue, boolean isAttributeValuePrefix);
	//public String getGrouppingAttributeName();
}
