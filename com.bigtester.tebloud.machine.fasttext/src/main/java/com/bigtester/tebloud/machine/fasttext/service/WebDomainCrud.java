/*******************************************************************************
 * ATE, Automation Test Engine
 *
 * Copyright 2015, Montreal PROT, or individual contributors as
 * indicated by the @author tags or express copyright attribution
 * statements applied by the authors.  All third-party contributions are
 * distributed under license by Montreal PROT.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package com.bigtester.tebloud.machine.fasttext.service;

import org.eclipse.jdt.annotation.Nullable;
import org.neo4j.ogm.session.Session;
import org.neo4j.ogm.transaction.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bigtester.ate.tcg.model.IntermediateResult;
import com.bigtester.ate.tcg.model.domain.AbstractScreenNode;
import com.bigtester.ate.tcg.model.domain.WebDomain;
import com.bigtester.ate.tcg.service.repository.PredictedFieldNameRepo;
import com.bigtester.ate.tcg.service.repository.ScreenNodeRepo;
import com.bigtester.ate.tcg.service.repository.TestCaseRepo;
import com.bigtester.ate.tcg.service.repository.TestSuiteRepo;
import com.bigtester.ate.tcg.service.repository.UserInputTrainingRecordRepo;
import com.bigtester.ate.tcg.service.repository.UserInputValueRepo;
import com.bigtester.ate.tcg.service.repository.WebDomainRepo;

// TODO: Auto-generated Javadoc
/**
 * This class ScreenNodeCrud defines ....
 * 
 * @author Peidong Hu
 *
 */
@Service
public class WebDomainCrud {

	/** The neo4j session. */
	@Autowired
	@Nullable
	private transient Session neo4jSession;

	/** The screen node repo. */
	@Autowired
	@Nullable
	private transient ScreenNodeRepo screenNodeRepo;

	/** The user input value repo. */
	@Autowired
	@Nullable
	private UserInputValueRepo userInputValueRepo;

	/** The predicted field name repo. */
	@Autowired
	@Nullable
	private PredictedFieldNameRepo predictedFieldNameRepo;

	/** The user input training record repo. */
	@Autowired
	@Nullable
	private UserInputTrainingRecordRepo userInputTrainingRecordRepo;

	/** The web domain repo. */
	@Autowired
	@Nullable
	private WebDomainRepo webDomainRepo;

	/** The test case repo. */
	@Autowired
	@Nullable
	private TestCaseRepo testCaseRepo;

	/** The test suite repo. */
	@Autowired
	@Nullable
	private TestSuiteRepo testSuiteRepo;

	/**
	 * @return the screenNodeRepo
	 */

	public ScreenNodeRepo getScreenNodeRepo() {

		final ScreenNodeRepo screenNodeRepo2 = screenNodeRepo;
		if (screenNodeRepo2 != null) {
			return screenNodeRepo2;
		} else {
			throw new IllegalStateException("screenNodeRepo");
		}
	}

	/**
	 * @param screenNodeRepo
	 *            the screenNodeRepo to set
	 */
	public void setScreenNodeRepo(ScreenNodeRepo screenNodeRepo) {
		this.screenNodeRepo = screenNodeRepo;
	}

	/**
	 * @return the userInputValueRepo
	 */
	public UserInputValueRepo getUserInputValueRepo() {
		final UserInputValueRepo userInputValueRepo2 = userInputValueRepo;
		if (userInputValueRepo2 != null) {
			return userInputValueRepo2;
		} else {
			throw new IllegalStateException("userInputValueRepo");
		}
	}

	/**
	 * @param userInputValueRepo
	 *            the userInputValueRepo to set
	 */
	public void setUserInputValueRepo(UserInputValueRepo userInputValueRepo) {
		this.userInputValueRepo = userInputValueRepo;
	}

	/**
	 * @return the predictedFieldNameRepo
	 */
	public PredictedFieldNameRepo getPredictedFieldNameRepo() {
		final PredictedFieldNameRepo predictedFieldNameRepo2 = predictedFieldNameRepo;
		if (predictedFieldNameRepo2 != null) {
			return predictedFieldNameRepo2;
		} else {
			// TODO handle null value
			throw new IllegalStateException("predictedFieldNameRepo");
		}
	}

	/**
	 * @param predictedFieldNameRepo
	 *            the predictedFieldNameRepo to set
	 */
	public void setPredictedFieldNameRepo(
			PredictedFieldNameRepo predictedFieldNameRepo) {
		this.predictedFieldNameRepo = predictedFieldNameRepo;
	}

	/**
	 * @return the userInputTrainingRecordRepo
	 */
	public UserInputTrainingRecordRepo getUserInputTrainingRecordRepo() {
		final UserInputTrainingRecordRepo userInputTrainingRecordRepo2 = userInputTrainingRecordRepo;
		if (userInputTrainingRecordRepo2 != null) {
			return userInputTrainingRecordRepo2;
		} else {
			// TODO handle null value
			throw new IllegalStateException("userInputTrainingRecordRepo");
		}
	}

	/**
	 * @param userInputTrainingRecordRepo
	 *            the userInputTrainingRecordRepo to set
	 */
	public void setUserInputTrainingRecordRepo(
			UserInputTrainingRecordRepo userInputTrainingRecordRepo) {
		this.userInputTrainingRecordRepo = userInputTrainingRecordRepo;
	}

	/**
	 * @return the webDomainRepo
	 */
	public WebDomainRepo getWebDomainRepo() {
		final WebDomainRepo webDomainRepo2 = webDomainRepo;
		if (webDomainRepo2 != null) {
			return webDomainRepo2;
		} else {
			// TODO handle null value
			throw new IllegalStateException("webDomainRepo");
		}
	}

	/**
	 * @param webDomainRepo
	 *            the webDomainRepo to set
	 */
	public void setWebDomainRepo(WebDomainRepo webDomainRepo) {
		this.webDomainRepo = webDomainRepo;
	}

	/**
	 * @return the testCaseRepo
	 */
	public TestCaseRepo getTestCaseRepo() {
		final TestCaseRepo testCaseRepo2 = testCaseRepo;
		if (testCaseRepo2 != null) {
			return testCaseRepo2;
		} else {
			// TODO handle null value
			throw new IllegalStateException("testCaseRepo");
		}
	}

	/**
	 * @param testCaseRepo
	 *            the testCaseRepo to set
	 */
	public void setTestCaseRepo(TestCaseRepo testCaseRepo) {
		this.testCaseRepo = testCaseRepo;
	}

	/**
	 * @return the testSuiteRepo
	 */
	public TestSuiteRepo getTestSuiteRepo() {
		final TestSuiteRepo testSuiteRepo2 = testSuiteRepo;
		if (testSuiteRepo2 != null) {
			return testSuiteRepo2;
		} else {
			// TODO handle null value
			throw new IllegalStateException("testSuiteRepo");
		}
	}

	/**
	 * @param testSuiteRepo
	 *            the testSuiteRepo to set
	 */
	public void setTestSuiteRepo(TestSuiteRepo testSuiteRepo) {
		this.testSuiteRepo = testSuiteRepo;
	}

	/**
	 * @return the neo4jSession
	 */
	public Session getNeo4jSession() {
		final Session neo4jSession2 = neo4jSession;
		if (neo4jSession2 == null) {
			throw new IllegalStateException("neo4j session");
		} else {
			return neo4jSession2;
		}
	}
	
	/**
	 * Create or update.
	 *
	 * @param intermediateResult the intermediate result
	 * @return the neo4j screen node
	 */
	public WebDomain createOrUpdate(IntermediateResult intermediateResult, boolean commit) {
		// save web domain
		WebDomain domainNode = getWebDomainRepo().getWebDomainByDomainName(
				intermediateResult.getDomainName());

		if (null == domainNode) {
			domainNode = new WebDomain(intermediateResult.getDomainName());// NOPMD
		} else {
			domainNode.setDomainName(intermediateResult.getDomainName());
		}
		if (commit) {
			//Transaction trx = getNeo4jSession().beginTransaction();
			try {
				domainNode = getWebDomainRepo().save(domainNode);
				//trx.commit();
				if (null == domainNode)
					throw new IllegalStateException("domainNode save");

			} finally {
				//trx.close();
			}
		}
		return domainNode;
	}
	
	/**
	 * Update.
	 *
	 * @param domainNode the domain node
	 * @return the web domain
	 */
	public WebDomain update(WebDomain domainNode) {

			WebDomain domainRet;
			//Transaction trx = getNeo4jSession().beginTransaction();
			try {
				domainRet = getWebDomainRepo().save(domainNode);
				//trx.commit();
				if (null == domainRet)
					throw new IllegalStateException("domainNode save");

			} finally {
				//trx.close();
			}
		
		return domainRet;
		
	}

	/**
	 * Update screen nodes.
	 *
	 * @param domainNode the domain node
	 * @param screenNode the screen node
	 * @param commit the commit
	 * @return the web domain
	 */
	public WebDomain updateScreenNodes(WebDomain domainNode, AbstractScreenNode screenNode, boolean commit) {
		if (!domainNode.getScreens().contains(screenNode))
			domainNode.getScreens().add(screenNode);
		if (commit)
			domainNode = update(domainNode);
		return domainNode;
	}
	
}
