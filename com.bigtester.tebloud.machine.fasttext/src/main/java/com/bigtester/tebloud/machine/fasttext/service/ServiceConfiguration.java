/*******************************************************************************
 * ATE, Automation Test Engine
 *
 * Copyright 2016, Montreal PROT, or individual contributors as
 * indicated by the @author tags or express copyright attribution
 * statements applied by the authors.  All third-party contributions are
 * distributed under license by Montreal PROT.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package com.bigtester.tebloud.machine.fasttext.service;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.eclipse.jdt.annotation.Nullable;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;

import com.bigtester.tebloud.machine.fasttext.service.uicollection.DivInSpan;
import com.bigtester.tebloud.machine.fasttext.service.uicollection.IUiInvisibilityExceptionalPattern;

/**
 * This class ServiceConfiguration defines ....
 * 
 * @author Peidong Hu
 *
 */

@Configuration
public class ServiceConfiguration {

	/** The logger. */
	@Nullable	// NOPMD
	private Log logger;

	/**
	 * Instantiates a new service configuration.
	 */
	public ServiceConfiguration() {
		super();
		Log logger = LogFactory.getLog(ServiceConfiguration.class);
		if (null == logger)
			throw new IllegalStateException("logger error");
		else
			setLogger(logger);

	}

//	/**
//	 * Div in span.
//	 *
//	 * @return the i ui invisibility exceptional pattern
//	 */
//	@Bean
//	@Scope("prototype")
//	public IUiInvisibilityExceptionalPattern divInSpan() {
//		getLogger().debug("enter divInSpan");
//
//		return new DivInSpan();
//	}

	/**
	 * @return the logger
	 */
	public Log getLogger() {
		final Log logger2 = logger;
		if (logger2 != null) {
			return logger2;
		} else {
			throw new IllegalStateException("logger error");
		}
	}

	/**
	 * @param logger
	 *            the logger to set
	 */
	public void setLogger(Log logger) {
		this.logger = logger;
	}
}
