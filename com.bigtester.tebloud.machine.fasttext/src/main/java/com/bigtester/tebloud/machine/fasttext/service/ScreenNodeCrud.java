/*******************************************************************************
 * ATE, Automation Test Engine
 *
 * Copyright 2015, Montreal PROT, or individual contributors as
 * indicated by the @author tags or express copyright attribution
 * statements applied by the authors.  All third-party contributions are
 * distributed under license by Montreal PROT.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package com.bigtester.tebloud.machine.fasttext.service;//NOPMD

import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import org.eclipse.jdt.annotation.Nullable;
import org.neo4j.ogm.model.Result;
import org.neo4j.ogm.session.Session;
import org.neo4j.ogm.transaction.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bigtester.ate.tcg.model.IntermediateResult;
import com.bigtester.ate.tcg.model.domain.Neo4jScreenNode;
import com.bigtester.ate.tcg.model.domain.ScreenElementChangeUITR;
import com.bigtester.ate.tcg.model.domain.ScreenJumperElementTrainingRecord;
import com.bigtester.ate.tcg.model.domain.InScreenJumperTrainingRecord;
import com.bigtester.ate.tcg.model.domain.UserInputTrainingRecord;
import com.bigtester.ate.tcg.model.domain.TestCase;
import com.bigtester.ate.tcg.model.domain.WebElementTrainingRecord;
import com.bigtester.ate.tcg.model.domain.WindowsSystemFilePickerScreenNode;
import com.bigtester.ate.tcg.model.relationship.InTestCase;
import com.bigtester.ate.tcg.model.relationship.StepOut;
import com.bigtester.ate.tcg.service.repository.PredictedFieldNameRepo;
import com.bigtester.ate.tcg.service.repository.ScreenNodeRepo;
import com.bigtester.ate.tcg.service.repository.TestCaseRepo;
import com.bigtester.ate.tcg.service.repository.TestSuiteRepo;
import com.bigtester.ate.tcg.service.repository.UserInputTrainingRecordRepo;
import com.bigtester.ate.tcg.service.repository.UserInputValueRepo;
import com.bigtester.ate.tcg.service.repository.WebDomainRepo;
import com.bigtester.ate.tcg.service.IScreenNodeCrud;
import com.bigtester.ate.tcg.service.repository.InTestCaseRelationshipRepo;
import com.bigtester.ate.tcg.service.repository.WindowsSystemFilePickerScreenNodeRepo;
import com.bigtester.ate.tcg.utils.GlobalUtils;

// TODO: Auto-generated Javadoc
/**
 * This class ScreenNodeCrud defines ....
 * 
 * @author Peidong Hu
 *
 */
@Service
public class ScreenNodeCrud implements IScreenNodeCrud {

	/** The neo4j session. */
	@Autowired
	@Nullable
	private transient Session neo4jSession;

	/** The screen node repo. */
	@Autowired
	@Nullable
	private transient ScreenNodeRepo screenNodeRepo;

	/** The user input value repo. */
	@Autowired
	@Nullable
	private UserInputValueRepo userInputValueRepo;

	/** The predicted field name repo. */
	@Autowired
	@Nullable
	private PredictedFieldNameRepo predictedFieldNameRepo;

	/** The user input training record repo. */
	@Autowired
	@Nullable
	private UserInputTrainingRecordRepo userInputTrainingRecordRepo;

	@Autowired
	@Nullable
	private InTestCaseRelationshipRepo InTestCaseRelationshipRepo;

	/** The windows file picker screen node repo. */
	@Autowired
	@Nullable
	private transient WindowsSystemFilePickerScreenNodeRepo windowsFilePickerScreenNodeRepo;
	@Autowired
	@Nullable
	private transient WindowsSystemFilePickerScreenNodeCrud windowsFilePickerScreenNodeCrud;
	/** The web domain repo. */
	@Autowired
	@Nullable
	private WebDomainRepo webDomainRepo;

	/** The test case repo. */
	@Autowired
	@Nullable
	private TestCaseRepo testCaseRepo;

	/** The test suite repo. */
	@Autowired
	@Nullable
	private TestSuiteRepo testSuiteRepo;

	/**
	 * @return the screenNodeRepo
	 */

	public ScreenNodeRepo getScreenNodeRepo() {

		final ScreenNodeRepo screenNodeRepo2 = screenNodeRepo;
		if (screenNodeRepo2 != null) {
			return screenNodeRepo2;
		} else {
			throw new IllegalStateException("screenNodeRepo");
		}
	}

	/**
	 * @param screenNodeRepo
	 *            the screenNodeRepo to set
	 */
	public void setScreenNodeRepo(ScreenNodeRepo screenNodeRepo) {
		this.screenNodeRepo = screenNodeRepo;
	}

	/**
	 * @return the userInputValueRepo
	 */
	public UserInputValueRepo getUserInputValueRepo() {
		final UserInputValueRepo userInputValueRepo2 = userInputValueRepo;
		if (userInputValueRepo2 != null) {
			return userInputValueRepo2;
		} else {
			throw new IllegalStateException("userInputValueRepo");
		}
	}

	/**
	 * @param userInputValueRepo
	 *            the userInputValueRepo to set
	 */
	public void setUserInputValueRepo(UserInputValueRepo userInputValueRepo) {
		this.userInputValueRepo = userInputValueRepo;
	}

	/**
	 * @return the predictedFieldNameRepo
	 */
	public PredictedFieldNameRepo getPredictedFieldNameRepo() {
		final PredictedFieldNameRepo predictedFieldNameRepo2 = predictedFieldNameRepo;
		if (predictedFieldNameRepo2 != null) {
			return predictedFieldNameRepo2;
		} else {
			// TODO handle null value
			throw new IllegalStateException("predictedFieldNameRepo");
		}
	}

	/**
	 * @param predictedFieldNameRepo
	 *            the predictedFieldNameRepo to set
	 */
	public void setPredictedFieldNameRepo(
			PredictedFieldNameRepo predictedFieldNameRepo) {
		this.predictedFieldNameRepo = predictedFieldNameRepo;
	}

	/**
	 * @return the userInputTrainingRecordRepo
	 */
	public UserInputTrainingRecordRepo getUserInputTrainingRecordRepo() {
		final UserInputTrainingRecordRepo userInputTrainingRecordRepo2 = userInputTrainingRecordRepo;
		if (userInputTrainingRecordRepo2 != null) {
			return userInputTrainingRecordRepo2;
		} else {
			// TODO handle null value
			throw new IllegalStateException("userInputTrainingRecordRepo");
		}
	}

	public boolean deleteInTestCaseRelationship(Long graphId) {
		// Transaction trx1 = getNeo4jSession().beginTransaction();
		// try {

		this.getInTestCaseRelationshipRepo().delete(graphId);
		// trx1.commit();

		// } finally {
		// trx1.close();
		// }
		InTestCase inTestCase = this.getInTestCaseRelationshipRepo().findOne(
				graphId);
		return inTestCase == null;
	}

	/**
	 * @param userInputTrainingRecordRepo
	 *            the userInputTrainingRecordRepo to set
	 */
	public void setUserInputTrainingRecordRepo(
			UserInputTrainingRecordRepo userInputTrainingRecordRepo) {
		this.userInputTrainingRecordRepo = userInputTrainingRecordRepo;
	}

	/**
	 * @return the webDomainRepo
	 */
	public WebDomainRepo getWebDomainRepo() {
		final WebDomainRepo webDomainRepo2 = webDomainRepo;
		if (webDomainRepo2 != null) {
			return webDomainRepo2;
		} else {
			// TODO handle null value
			throw new IllegalStateException("webDomainRepo");
		}
	}

	/**
	 * @param webDomainRepo
	 *            the webDomainRepo to set
	 */
	public void setWebDomainRepo(WebDomainRepo webDomainRepo) {
		this.webDomainRepo = webDomainRepo;
	}

	/**
	 * @return the testCaseRepo
	 */
	public TestCaseRepo getTestCaseRepo() {
		final TestCaseRepo testCaseRepo2 = testCaseRepo;
		if (testCaseRepo2 != null) {
			return testCaseRepo2;
		} else {
			// TODO handle null value
			throw new IllegalStateException("testCaseRepo");
		}
	}

	/**
	 * @param testCaseRepo
	 *            the testCaseRepo to set
	 */
	public void setTestCaseRepo(TestCaseRepo testCaseRepo) {
		this.testCaseRepo = testCaseRepo;
	}

	/**
	 * @return the testSuiteRepo
	 */
	public TestSuiteRepo getTestSuiteRepo() {
		final TestSuiteRepo testSuiteRepo2 = testSuiteRepo;
		if (testSuiteRepo2 != null) {
			return testSuiteRepo2;
		} else {
			// TODO handle null value
			throw new IllegalStateException("testSuiteRepo");
		}
	}

	/**
	 * @param testSuiteRepo
	 *            the testSuiteRepo to set
	 */
	public void setTestSuiteRepo(TestSuiteRepo testSuiteRepo) {
		this.testSuiteRepo = testSuiteRepo;
	}

	/**
	 * @return the neo4jSession
	 */
	public Session getNeo4jSession() {
		final Session neo4jSession2 = neo4jSession;
		if (neo4jSession2 == null) {
			throw new IllegalStateException("neo4j session");
		} else {
			return neo4jSession2;
		}
	}

	/**
	 * Gets the screen node by name url and domain name.
	 *
	 * @param screenName the screen name
	 * @param screenUrl the screen url
	 * @param domainName the domain name
	 * @return the screen node by name url and domain name
	 */
	public Optional<Neo4jScreenNode> getScreenNodeByNameUrlAndDomainName(String screenName, String screenUrl, String domainName) {
		Iterable<Long> screenIds = this.getScreenNodeRepo().getNeo4jScreenNodeIdByUrlNameAndDomainName(screenUrl, screenName, domainName);
		if (screenIds.iterator().hasNext()) {
			return Optional.of(this.getScreenNodeRepo().findOne(screenIds.iterator().next(), 2));
		} else {
			return Optional.empty();
		}
	}
	/**
	 * Create or update.
	 *
	 * @param intermediateResult
	 *            the intermediate result
	 * @return the neo4j screen node
	 */
	public Neo4jScreenNode createOrUpdate(
			IntermediateResult intermediateResult, boolean commit) {

		// save screen node
		Neo4jScreenNode prevousScreenNode = null;// NOPMD
		IntermediateResult previousIntermediateResult = intermediateResult
				.getLastScreenNodeIntermediateResult();
		if (null != previousIntermediateResult
				&& !previousIntermediateResult.getScreenNode().getName().equals("")) {// &&

			prevousScreenNode = previousIntermediateResult.getScreenNode();

		}
		Neo4jScreenNode currentNode=null;
		if (intermediateResult.getScreenNode().getGraphId() == null
				|| intermediateResult.getScreenNode().getGraphId()== 0) {
			if (intermediateResult.isForceCreateNewScreen()) {
				currentNode = null; 
			} else {
				Optional<Neo4jScreenNode> possibleExistingNode = getScreenNodeByNameUrlAndDomainName(intermediateResult.getScreenNode().getName(), intermediateResult.getScreenNode().getUrl(), intermediateResult.getDomainName());
				currentNode = possibleExistingNode.orElse(null);				
			}
			if (null == currentNode) {
				currentNode = intermediateResult.getScreenNode();	
			} else {
				intermediateResult.setSamePageUpdate(true);
				currentNode
				.populateCorrectValueforFieldBelongsToTestCaseFromDBInfo(intermediateResult.getTestCaseName());
				currentNode.setName(intermediateResult.getScreenNode().getName());
				
				currentNode.setSourcingDoms(intermediateResult.getScreenNode().getSourcingDoms());
				currentNode.setInputMLHtmlWords(intermediateResult.getScreenNode().getInputMLHtmlWords()
						);
				currentNode.setUserInputUitrs(intermediateResult.getScreenNode()
						.getUserInputUitrs());
				currentNode.setClickUitrs(intermediateResult.getScreenNode().getClickUitrs()
						);
				// Bug here, a save will generate new action uitrs in update
				// scenario
				currentNode.setActionUitrs(intermediateResult.getScreenNode().getActionUitrs());
				currentNode.setScreenElementChangeUitrs(intermediateResult.getScreenNode().getScreenElementChangeUitrs()
						);
				currentNode.setUrl(intermediateResult.getScreenNode().getUrl());
				currentNode.setUitrTrainedEntityIds(intermediateResult.getScreenNode()
						.getUitrTrainedEntityIds());
				currentNode.setDecisionUitrTrainedEntityIds(intermediateResult.getScreenNode()
						.getDecisionUitrTrainedEntityIds());
				currentNode.setScreenTrainedEntityId(intermediateResult.getScreenNode()
						.getScreenTrainedEntityId());
				// set operation only do add and update operations on the left
				// object. duplicated uitrs could be introduced here in the screen
				// node.
				// an example case, if the ui changed the userinputtype for uitr
				// from userinput to screnelementchanger type in a samepageupdate
				// call,
				// same graphid node will exists both in userinputuitr and
				// screenelementchangeuitrs relationships. causing graph modeling
				// problem.
				// use this function to eliminate this problem,
				//NOTE: above scenario should not exists as we have add delete node relationship call when switch userinput type
				//currentNode.removeDuplidatedUitrs(intermediateResult);

			}

		} 
		else {

			currentNode = intermediateResult.getScreenNode();
		}

			
		// TODO hard code the elementChangedScreen to currentScreen. Will change
		// in the future when there is a case.
		final Neo4jScreenNode currentNodeForLamda = currentNode;
		currentNode.getScreenElementChangeUitrs().forEach(
				screenElementChangeUitr -> {
					// ScreenElementChangeUITR temp = screenElementChangeUitr;
					if (null == screenElementChangeUitr
							.getElementChangedScreen())
						screenElementChangeUitr
								.setElementChangedScreen(currentNodeForLamda.getGraphId());

				});

		if (commit || intermediateResult.isSamePageUpdate()) {
			// Transaction trx1 = getNeo4jSession().beginTransaction();
			try {
				// System.out.println(GlobalUtils.printObject(currentNode));
				// Bug here, a save will generate new action uitrs in update
				// scenario
				currentNode = getScreenNodeRepo().save(currentNode);

				// trx1.commit();
				currentNode = getScreenNodeRepo().findOne(
						currentNode.getGraphId(), 2);
				
				//make sure all in_testcase relationship correct for nodes retrieved from db (but not exist in ui)
				currentNode.populateCorrectValueforFieldBelongsToTestCaseFromDBInfo(intermediateResult.getTestCaseName());
				//make sure all in_testcase relationhip correct, and get ready for setting in_testcase relationship in the next step.
				currentNode.populateCorrectValueforFieldBelongsToTestCaseFromIntermediateResult(intermediateResult);

				if (null == currentNode || currentNode.getId() == null)
					throw new IllegalStateException("neo4j db  access");
				// TODO refresh intermediateResult
				intermediateResult.setScreenNode(currentNode);
			} finally {
				// trx1.close();
			}
		}
		if (null != previousIntermediateResult && null != prevousScreenNode) {// &&
																				// !intermediateResult.isSamePageUpdate())
																				// {
			// TODO check if intermediateResult and previousIntermediateResult
			// needs to be refresshed
			createOrUpdateStepout(prevousScreenNode, currentNode,
					intermediateResult);
			// TODO check if intermediateResult and previousIntermediateResult
			// needs to be refresshed
			updateTestCaseRelationships(previousIntermediateResult,
					false);
			if (previousIntermediateResult.getScreenNode().getScreenType().equals(
					Neo4jScreenNode.ScreenType.WINDOWFILEPICKER)
					&& prevousScreenNode instanceof WindowsSystemFilePickerScreenNode) {
				WindowsSystemFilePickerScreenNode tmp = this
						.getWindowsFilePickerScreenNodeCrud()
						.update((WindowsSystemFilePickerScreenNode) prevousScreenNode);
				// TODO abstract a intermediateResult refresh function
				if (!(tmp == null || tmp.getId() == null)) {
					intermediateResult.getLastScreenNodeIntermediateResult()
							.getScreenNode().setActionUitrs(prevousScreenNode.getActionUitrs());
					intermediateResult.getLastScreenNodeIntermediateResult().getScreenNode()
							.setName(prevousScreenNode.getName());
					intermediateResult.getLastScreenNodeIntermediateResult().getScreenNode().setSourcingDoms(
							prevousScreenNode.getSourcingDoms());
					intermediateResult.getLastScreenNodeIntermediateResult().getScreenNode()
							.setUserInputUitrs(
									prevousScreenNode.getUserInputUitrs());
					intermediateResult.getLastScreenNodeIntermediateResult().getScreenNode().setClickUitrs(
									prevousScreenNode.getClickUitrs());
					intermediateResult.getLastScreenNodeIntermediateResult().getScreenNode().setScreenElementChangeUitrs(
									prevousScreenNode
											.getScreenElementChangeUitrs());
					intermediateResult.getLastScreenNodeIntermediateResult().getScreenNode()
							.setUrl(prevousScreenNode.getUrl());
					intermediateResult.getLastScreenNodeIntermediateResult().getScreenNode()
							.setScreenTrainedEntityId(
									prevousScreenNode
											.getScreenTrainedEntityId());
					intermediateResult
							.getLastScreenNodeIntermediateResult().getScreenNode()
							.setUitrTrainedEntityIds(
									prevousScreenNode.getUitrTrainedEntityIds());
					intermediateResult.getLastScreenNodeIntermediateResult().getScreenNode()
							.setDecisionUitrTrainedEntityIds(
									prevousScreenNode
											.getDecisionUitrTrainedEntityIds());
				}
			} else {
				Neo4jScreenNode tmp = update(prevousScreenNode);
				tmp = getScreenNodeRepo().findOne(
						tmp.getGraphId(), 2);
				
				//make sure all in_testcase relationship correct for nodes retrieved from db (but not exist in ui)
				tmp.populateCorrectValueforFieldBelongsToTestCaseFromDBInfo(intermediateResult.getTestCaseName());
				// refressh previousIntermediateResult.
				previousIntermediateResult.setScreenNode(tmp);
//				if (!(tmp == null || tmp.getId() == null)) {
//					intermediateResult.getLastScreenNodeIntermediateResult().getScreenNode()
//							.setActionUitrs(prevousScreenNode.getActionUitrs());
//					intermediateResult.getLastScreenNodeIntermediateResult().getScreenNode()
//							.setName(prevousScreenNode.getName());
//					intermediateResult.getLastScreenNodeIntermediateResult().getScreenNode()
//							.setSourcingDoms(prevousScreenNode.getSourcingDoms());
//					intermediateResult.getLastScreenNodeIntermediateResult().getScreenNode()
//							.setUserInputUitrs(
//									prevousScreenNode.getUserInputUitrs());
//					intermediateResult.getLastScreenNodeIntermediateResult().getScreenNode()
//							.setClickUitrs(
//									prevousScreenNode.getClickUitrs());
//					intermediateResult.getLastScreenNodeIntermediateResult().getScreenNode()
//							.setScreenElementChangeUitrs(
//									prevousScreenNode
//											.getScreenElementChangeUitrs());
//					intermediateResult.getLastScreenNodeIntermediateResult().getScreenNode()
//							.setUrl(prevousScreenNode.getUrl());
//					intermediateResult.getLastScreenNodeIntermediateResult().getScreenNode()
//							.setScreenTrainedEntityId(
//									prevousScreenNode
//											.getScreenTrainedEntityId());
//					intermediateResult
//							.getLastScreenNodeIntermediateResult().getScreenNode()
//							.setUitrTrainedEntityIds(
//									prevousScreenNode.getUitrTrainedEntityIds());
//					intermediateResult.getLastScreenNodeIntermediateResult().getScreenNode()
//							.setDecisionUitrTrainedEntityIds(
//									prevousScreenNode
//											.getDecisionUitrTrainedEntityIds());
//				}
			}

		}

		return currentNode;
	}

	/**
	 * Update.
	 *
	 * @param screenNode
	 *            the screen node
	 * @return the neo4j screen node
	 */
	public Neo4jScreenNode update(Neo4jScreenNode screenNode) {
		Neo4jScreenNode tmp;
		// Transaction trx = getNeo4jSession().beginTransaction();//NOPMD
		try {

			tmp = getScreenNodeRepo().save(screenNode);
			//tmp = getScreenNodeRepo().findOne(tmp.getGraphId(),2);
			// trx.commit();
		} finally {
			// trx.close();
		}

		return screenNode;
	}

	/**
	 * Update test case relationships.
	 *
	 * @param screenNode
	 *            the screen node
	 * @param intermediateResult
	 *            the intermediate result
	 * @param commit
	 *            the commit
	 * @return the neo4j screen node
	 */
	public Neo4jScreenNode updateTestCaseRelationships(
			IntermediateResult intermediateResult,
			boolean commit) {
		Neo4jScreenNode screenNode = intermediateResult.getScreenNode();
		// save test case

		// TestSuite tmpSuite =
		// intermediateResult.getTestSuitesMap().get(intermediateResult.getTestSuitesMap().size()
		// - 1);
		// TestSuite existingSuite =
		// getTestSuiteRepo().getTestSuiteByName(tmpSuite.getName());
		//
		// if (null != existingSuite) {
		// tmpSuite = existingSuite;
		// }
		// TestCase testcaseNode = getTestCaseRepo().getTestCaseByName(
		// intermediateResult.getTestCaseName());
		//
		//
		// if (null == testcaseNode) {
		//
		// testcaseNode = new TestCase(intermediateResult.getTestCaseName(),
		// tmpSuite);// NOPMD
		//
		// } else {
		// testcaseNode.setName(intermediateResult.getTestCaseName());
		// if (!testcaseNode.getHostingTestSuites().contains(tmpSuite))
		// testcaseNode.getHostingTestSuites().add(tmpSuite);
		// }
		TestCase testcaseNode = getTestCaseRepo().getTestCaseByName(
				intermediateResult.getTestCaseName(),2);
		if (null == testcaseNode)
			throw new IllegalStateException(
					"please update/create test case node first");
		// if (!screenNode.getTestcases().contains(testcaseNode))
		// screenNode.getTestcases().add(testcaseNode);
		screenNode.addIntoTestCase(screenNode, testcaseNode);

		List<UserInputTrainingRecord> userInputUitrs = screenNode
				.getUserInputUitrs();
		for (java.util.Iterator<UserInputTrainingRecord> itr = userInputUitrs
				.iterator(); itr.hasNext();) {
			UserInputTrainingRecord uitr = itr.next();
			if (uitr.isBelongToCurrentTestCase()) {
				uitr.addIntoTestCase(uitr, testcaseNode, screenNode);
			} else {
				uitr.removeFromTestCase(uitr, testcaseNode, screenNode);
			}
		}

		List<InScreenJumperTrainingRecord> clickUitrs = screenNode
				.getClickUitrs();
		for (java.util.Iterator<InScreenJumperTrainingRecord> itr = clickUitrs
				.iterator(); itr.hasNext();) {
			InScreenJumperTrainingRecord uitr = itr.next();
			if (uitr.isBelongToCurrentTestCase()) {
				uitr.addIntoTestCase(uitr, testcaseNode, screenNode);
			} else {
				uitr.removeFromTestCase(uitr, testcaseNode, screenNode);
			}
		}

		List<ScreenJumperElementTrainingRecord> actionUitrs = screenNode
				.getActionUitrs();
		for (java.util.Iterator<ScreenJumperElementTrainingRecord> itr = actionUitrs
				.iterator(); itr.hasNext();) {
			ScreenJumperElementTrainingRecord uitr = itr.next();
			if (uitr.isBelongToCurrentTestCase()) {
				uitr.addIntoTestCase(uitr, testcaseNode, screenNode);
			} else {
				uitr.removeFromTestCase(uitr, testcaseNode, screenNode);
			}
		}

		List<ScreenElementChangeUITR> screenChangeUitrs = screenNode
				.getScreenElementChangeUitrs();
		for (java.util.Iterator<ScreenElementChangeUITR> itr = screenChangeUitrs
				.iterator(); itr.hasNext();) {
			ScreenElementChangeUITR uitr = itr.next();
			if (uitr.isBelongToCurrentTestCase()) {
				uitr.addIntoTestCase(uitr, testcaseNode, screenNode);
			} else {
				uitr.removeFromTestCase(uitr, testcaseNode, screenNode);
			}
		}
		if (!commit)
			return screenNode;// NOPMD
		else {
			
			return update(screenNode);
		}
	}

	private void createOrUpdateStepoutForScreenJumper(
			Neo4jScreenNode startNode, Neo4jScreenNode endNode,
			IntermediateResult iResult, UserInputTrainingRecord screenJumperUitr) {

		if (null != screenJumperUitr) {
			// create
			if (screenJumperUitr instanceof InScreenJumperTrainingRecord)
				screenJumperUitr.addStepOut(endNode,
						StepOut.StepOutTriggerType.INSCREENJUMPERSTEPOUT);
			// else
			screenJumperUitr.addStepOut(endNode,
					StepOut.StepOutTriggerType.SCREENJUMPERSTEPOUT);

		}
	}

	@Nullable
	private UserInputTrainingRecord getStartNodeTriggerUitr(
			Neo4jScreenNode startNode, Long triggerUitrId) {

		List<InScreenJumperTrainingRecord> inScreenJumperUitrs = startNode
				.getClickUitrs();
		List<InScreenJumperTrainingRecord> inScreenJumperTriggers = inScreenJumperUitrs
				.stream()
				.filter(uitr -> uitr.isActionTrigger()
						&& uitr.getTestcases().size() > 0
						&& uitr.getGraphId().equals(triggerUitrId))
				.collect(Collectors.toList());

		List<ScreenJumperElementTrainingRecord> startActionUitrs = startNode
				.getActionUitrs();
		List<ScreenJumperElementTrainingRecord> triggers = startActionUitrs
				.stream()
				.filter(uitr -> uitr.isActionTrigger()
						&& uitr.getTestcases().size() > 0
						&& uitr.getGraphId().equals(triggerUitrId))
				.collect(Collectors.toList());

		if (inScreenJumperTriggers.size() == 0 && triggers.size() == 0) {
			throw new IllegalStateException(
					"# of screen jumper (or in screen jumper) is 0");
		}

		if (inScreenJumperTriggers.size() > 1)
			throw new IllegalStateException(
					"# of trigger InScreenJumper uitrs is greater than 1.");

		if (triggers.size() > 1)
			throw new IllegalStateException(
					"# of start action uitrs is greater than 1.");

		if (inScreenJumperTriggers.size() == 1 && triggers.size() == 1)
			throw new IllegalStateException(
					"can only inScreenJumper or screemJumper is action trigger in one screen step.");
		else if (inScreenJumperTriggers.size() == 1)
			return inScreenJumperTriggers.iterator().next();
		else
			return triggers.iterator().next();

	}

	/**
	 * Stepped into.
	 *
	 * @param endNode
	 *            the end node
	 * @param uitrId
	 *            the uitr id
	 * @return the step into
	 */
	public void createOrUpdateStepout(Neo4jScreenNode startNode,
			Neo4jScreenNode endNode, IntermediateResult iResult) {
		// TODO, add test case filter after finish job application code

		UserInputTrainingRecord trigger = this.getStartNodeTriggerUitr(
				startNode, iResult.getScreenTriggeredByPreviousScreenUitrId());
		if (null == trigger)
			throw new IllegalStateException(
					"# of trigger action uitr in start screen is 0");
		createOrUpdateStepoutForScreenJumper(startNode, endNode, iResult,
				trigger);

	}

	// MATCH
	// (screen:Neo4jScreenNode{name:'jobDescApplyPage'})-[jumper_r:PREDICTED_USER_SCREENJUMP_ELEMENT_RESULTS]-(jumper)-[inTest:IN_TESTCASE]-()
	// RETURN distinct screen.name, jumper.inputLabelName
	/**
	 * Find distinct screen jumper label names in test case.
	 *
	 * @param screenName
	 *            the screen name
	 * @param testCase
	 *            the test case
	 * @return the iterable
	 */
	public Iterable<String> findDistinctScreenJumperLabelNamesInTestCase(
			String screenName, TestCase testCase) {
		return this.getScreenNodeRepo().getDistinctScreenJumperLabelNames(
				screenName);
	}

	/**
	 * Match screen jumper label names in test case.
	 *
	 * @param screenName
	 *            the screen name
	 * @param labelNameToMatch
	 *            the label name to match
	 * @param testCase
	 *            the test case
	 * @return true, if successful
	 */
	public boolean matchScreenJumperLabelNamesInTestCase(String screenName,
			final String labelNameToMatch, TestCase testCase) {
		return StreamSupport.stream(
				this.getScreenNodeRepo()
						.getDistinctScreenJumperLabelNames(screenName)
						.spliterator(), true).anyMatch(
				labelInDb -> labelInDb.equalsIgnoreCase(labelNameToMatch));
	}

	public boolean matchNonActionUserInputLabelNamesInTestCase(
			String screenName, final String labelNameToMatch, TestCase testCase) {
		return StreamSupport.stream(
				this.getScreenNodeRepo()
						.getDistinctNonActionUserInputLabelNames(screenName)
						.spliterator(), true).anyMatch(
				labelInDb -> labelInDb.equalsIgnoreCase(labelNameToMatch));
	}

	/**
	 * Match screen element changer label names in test case.
	 *
	 * @param screenName
	 *            the screen name
	 * @param labelNameToMatch
	 *            the label name to match
	 * @param testCase
	 *            the test case
	 * @return true, if successful
	 */
	public boolean matchScreenElementChangerLabelNamesInTestCase(
			String screenName, final String labelNameToMatch, TestCase testCase) {
		return StreamSupport.stream(
				this.getScreenNodeRepo()
						.getDistinctScreenElementChangerLabelNames(screenName)
						.spliterator(), true).anyMatch(
				labelInDb -> labelInDb.equalsIgnoreCase(labelNameToMatch));
	}

	/**
	 * Match in screen jumper label names in test case.
	 *
	 * @param screenName
	 *            the screen name
	 * @param labelNameToMatch
	 *            the label name to match
	 * @param testCase
	 *            the test case
	 * @return true, if successful
	 */
	public boolean matchInScreenJumperLabelNamesInTestCase(String screenName,
			final String labelNameToMatch, TestCase testCase) {
		return StreamSupport.stream(
				this.getScreenNodeRepo()
						.getDistinctInScreenJumperLabelNames(screenName)
						.spliterator(), true).anyMatch(
				labelInDb -> labelInDb.equalsIgnoreCase(labelNameToMatch));
	}

	/**
	 * Find distinct user input label names in test case.
	 *
	 * @param screenName
	 *            the screen name
	 * @param testCase
	 *            the test case
	 * @return the iterable
	 */
	public Iterable<String> findDistinctUserInputLabelNamesInTestCase(
			String screenName, TestCase testCase) {
		return this.getScreenNodeRepo().getDistinctUserInputLabelNames(
				screenName);
	}

	/**
	 * Find distinct end screen names in test case.
	 *
	 * @param testCase
	 *            the test case
	 * @return the iterable
	 */
	public Iterable<String> findDistinctEndScreenNamesInTestCase(
			TestCase testCase) {
		return this.getScreenNodeRepo().getDistinctEndScreenNamesInTestCase();
	}

	/**
	 * @param neo4jSession
	 *            the neo4jSession to set
	 */
	public void setNeo4jSession(Session neo4jSession) {
		this.neo4jSession = neo4jSession;
	}

	/**
	 * @return the windowsFilePickerScreenNodeRepo
	 */
	public WindowsSystemFilePickerScreenNodeRepo getWindowsFilePickerScreenNodeRepo() {
		final WindowsSystemFilePickerScreenNodeRepo windowsFilePickerScreenNodeRepo2 = windowsFilePickerScreenNodeRepo;
		if (windowsFilePickerScreenNodeRepo2 != null) {
			return windowsFilePickerScreenNodeRepo2;
		} else {
			throw new IllegalStateException(
					"WindowsSystemFilePickerScreenNodeRepo");
		}
	}

	/**
	 * @param windowsFilePickerScreenNodeRepo
	 *            the windowsFilePickerScreenNodeRepo to set
	 */
	public void setWindowsFilePickerScreenNodeRepo(
			WindowsSystemFilePickerScreenNodeRepo windowsFilePickerScreenNodeRepo) {
		this.windowsFilePickerScreenNodeRepo = windowsFilePickerScreenNodeRepo;
	}

	/**
	 * @return the windowsFilePickerScreenNodeCrud
	 */
	public WindowsSystemFilePickerScreenNodeCrud getWindowsFilePickerScreenNodeCrud() {
		final WindowsSystemFilePickerScreenNodeCrud windowsFilePickerScreenNodeCrud2 = windowsFilePickerScreenNodeCrud;
		if (windowsFilePickerScreenNodeCrud2 != null) {
			return windowsFilePickerScreenNodeCrud2;
		} else {
			throw new IllegalStateException(
					"WindowsSystemFilePickerScreenNodeCrud");
		}
	}

	/**
	 * @param windowsFilePickerScreenNodeCrud
	 *            the windowsFilePickerScreenNodeCrud to set
	 */
	public void setWindowsFilePickerScreenNodeCrud(
			WindowsSystemFilePickerScreenNodeCrud windowsFilePickerScreenNodeCrud) {
		this.windowsFilePickerScreenNodeCrud = windowsFilePickerScreenNodeCrud;
	}

	/**
	 * @return the inTestCaseRelationshipRepo
	 */
	public InTestCaseRelationshipRepo getInTestCaseRelationshipRepo() {
		return InTestCaseRelationshipRepo;
	}

	/**
	 * @param inTestCaseRelationshipRepo
	 *            the inTestCaseRelationshipRepo to set
	 */
	public void setInTestCaseRelationshipRepo(
			InTestCaseRelationshipRepo inTestCaseRelationshipRepo) {
		InTestCaseRelationshipRepo = inTestCaseRelationshipRepo;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Double getMlHtmlCodeVsDomDocPercentileLengthRate(double rateRange) {
		return this.getScreenNodeRepo()
				.getMlHtmlCodeVsDomDocPercentileLengthRate(rateRange);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Double getMinMlHtmlCodeVsDomDocPercentileLengthRate() {
		return this.getScreenNodeRepo()
				.getMinMlHtmlCodeVsDomDocPercentileLengthRate();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Double getMaxMlHtmlCodeVsDomDocPercentileLengthRate() {
		return this.getScreenNodeRepo()
				.getMaxMlHtmlCodeVsDomDocPercentileLengthRate();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Double getMinElementSize(String elementLabel) {
		// TODO Auto-generated method stub
		return this.getScreenNodeRepo().getMinElementSize(elementLabel);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Double getMaxElementSize(String elementLabel) {
		// TODO Auto-generated method stub
		return this.getScreenNodeRepo().getMaxElementSize(elementLabel);
	}
	@Override
	public boolean deleteFieldGroupRel(Long screenNodeId) {
		return this.getScreenNodeRepo().deleteFieldGroupingRel(screenNodeId).queryStatistics().getRelationshipsDeleted()>0;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Double getElementSizePercentile(String elementLabel, double rateRange) {
		// TODO Auto-generated method stub
		return this.getScreenNodeRepo().getElementSizePercentileDist(
				elementLabel, rateRange);
	}

}
