/*******************************************************************************
 * ATE, Automation Test Engine
 *
 * Copyright 2016, Montreal PROT, or individual contributors as
 * indicated by the @author tags or express copyright attribution
 * statements applied by the authors.  All third-party contributions are
 * distributed under license by Montreal PROT.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package com.bigtester.tebloud.machine.fasttext.service.uicollection;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import static org.joox.JOOX.*;
// TODO: Auto-generated Javadoc
/**
 * This class DivInSpan defines ....
 * 
 * @author Peidong Hu
 *
 */
public class VisibleLabelForInvisibleInput implements
		IUiInvisibilityExceptionalPattern {

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean isFalseInvisible(Document doc, Node node,
			String invisibleAttName, String invisibleAttValue) {
		boolean retVal = false;
		if (node instanceof Element
				&& ((Element) node).getTagName().equalsIgnoreCase("input")
				&& ((Element) node).getAttribute(invisibleAttName)
						.equalsIgnoreCase(invisibleAttValue)) {
			String idS = ((Element) node).getAttribute("id");
			if (idS != null ) {
				Element label = $(doc).find("label[for='"+idS+"']").get(0);
				if (label !=null) {
					if (!label.getAttribute(invisibleAttName).equalsIgnoreCase(invisibleAttValue))
						retVal = true;
				}
			}
		}
		return retVal;
	}

}
