/*******************************************************************************
 * ATE, Automation Test Engine
 *
 * Copyright 2016, Montreal PROT, or individual contributors as
 * indicated by the @author tags or express copyright attribution
 * statements applied by the authors.  All third-party contributions are
 * distributed under license by Montreal PROT.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package com.bigtester.tebloud.machine.fasttext.service.uicollection.matcher;

import static org.joox.JOOX.$;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

import com.google.common.collect.Iterables;

// TODO: Auto-generated Javadoc
/**
 * This class OutOfFormInputListMatcher defines ....
 * @author Peidong Hu
 *
 */
public class OutOfFormInputListMatcher implements IUitrOnScreenLabelMatcher {

	final private static String HTML_TAG_UNORDEREDLIST = "ul";
	final private static String HTML_TAG_ORDEREDLIST = "ol";
	final private static String HTML_TAG_LISTITEM = "li";
	final private static String HTML_TAG_DESCRIPTIONLIST = "dl";
	final private static String HTML_TAG_DESCRIPTIONLISTTERM = "dt";
	final private static String HTML_TAG_DESCRIPTIONLISTDESCRIPTION = "dd";
	
	/**
	 * 
	 */
	public OutOfFormInputListMatcher() {
		// TODO Auto-generated constructor stub
	}

	private Optional<Node> getListNode(Node childNode) {
		Optional<Node> retVal = Optional.empty();
		List<Element> parents = $(childNode)
				.parentsUntil(HTML_TAG_UNORDEREDLIST).parent().get();
		
		if (parents.isEmpty() || Iterables
				.get(parents,
						parents.size() - 1)
				.getNodeName().equalsIgnoreCase("html")) {
			parents = $(childNode)
					.parentsUntil(HTML_TAG_ORDEREDLIST).parent().get();
			if (parents.isEmpty() || Iterables
					.get(parents,
							parents.size() - 1)
					.getNodeName().equalsIgnoreCase("html")) {
				parents = $(childNode)
						.parentsUntil(HTML_TAG_DESCRIPTIONLIST).parent().get();
					
			}		
		}
		if (!(parents.isEmpty() || Iterables
				.get(parents,
						parents.size() - 1)
				.getNodeName().equalsIgnoreCase("html"))) {
			
			retVal = Optional.of(parents.get(parents.size() - 1));
		}
		return retVal;
	}
	private Optional<Node> getListItemNode(Node childNode) {
		Optional<Node> retVal = Optional.empty();
		List<Element> parents = $(childNode)
				.parentsUntil(HTML_TAG_LISTITEM).parent().get();
		
		if (parents.isEmpty() || Iterables
				.get(parents,
						parents.size() - 1)
				.getNodeName().equalsIgnoreCase("html")) {
			parents = $(childNode)
					.parentsUntil(HTML_TAG_DESCRIPTIONLISTTERM).parent().get();
			if (parents.isEmpty() || Iterables
					.get(parents,
							parents.size() - 1)
					.getNodeName().equalsIgnoreCase("html")) {
				parents = $(childNode)
						.parentsUntil(HTML_TAG_DESCRIPTIONLISTDESCRIPTION).parent().get();
					
			}		
		}
		if (!(parents.isEmpty() || Iterables
				.get(parents,
						parents.size() - 1)
				.getNodeName().equalsIgnoreCase("html"))) {
			
			retVal = Optional.of(parents.get(parents.size() - 1));
		}
		return retVal;
	}
	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<Node> matchPattern(Document doc, Node matchNode) {
		List<? extends Node> retVal = new ArrayList<Node>();
		Optional<Node> listItemNode = getListItemNode(matchNode);
		retVal = listItemNode.map(li -> $(li).find("label").get()).orElse(new ArrayList<Element>());
		return (List<Node>) retVal;
	}

	

}
