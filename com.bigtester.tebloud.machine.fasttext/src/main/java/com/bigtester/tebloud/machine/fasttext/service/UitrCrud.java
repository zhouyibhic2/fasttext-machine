/*******************************************************************************
 * ATE, Automation Test Engine
 *
 * Copyright 2015, Montreal PROT, or individual contributors as
 * indicated by the @author tags or express copyright attribution
 * statements applied by the authors.  All third-party contributions are
 * distributed under license by Montreal PROT.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package com.bigtester.tebloud.machine.fasttext.service;

import java.util.Collections;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;
import org.eclipse.jdt.annotation.Nullable;
import org.neo4j.ogm.session.Session;
import org.neo4j.ogm.transaction.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bigtester.ate.tcg.model.IntermediateResult;
import com.bigtester.ate.tcg.model.domain.Neo4jScreenNode;
import com.bigtester.ate.tcg.model.domain.ScreenJumperElementTrainingRecord;
import com.bigtester.ate.tcg.model.domain.TestCase;
import com.bigtester.ate.tcg.model.domain.TestSuite;
import com.bigtester.ate.tcg.model.domain.UserInputTrainingRecord;
import com.bigtester.ate.tcg.service.repository.TestCaseRepo;
import com.bigtester.ate.tcg.service.repository.TestSuiteRepo;
import com.bigtester.ate.tcg.service.repository.UserInputTrainingRecordRepo;
import com.google.common.collect.Iterables;
import com.google.common.collect.Iterators;

// TODO: Auto-generated Javadoc
/**
 * This class ScreenNodeCrud defines ....
 * 
 * @author Peidong Hu
 *
 */
@Service
public class UitrCrud {

	/** The neo4j session. */
	@Autowired
	@Nullable
	private transient Session neo4jSession;

	/** The test case repo. */
	@Autowired
	@Nullable
	private UserInputTrainingRecordRepo uitrRepo;

	public Iterable<UserInputTrainingRecord> getAllSaveFieldLabeledUitrs(String domainName, String testCaseName, String label) {
		
		
		Iterable<UserInputTrainingRecord> allSameFieldUitrs1 = getUitrRepo()
					.findAllUitrsByPioPredictLabelResultValue(StringUtils.isEmpty(domainName)?".*":domainName,
							StringUtils.isEmpty(testCaseName)?".*":testCaseName, ".*", label);

		Iterable<UserInputTrainingRecord> allSameFieldUitrs2 = getUitrRepo()
			.findAllUitrsByUserInputLabelNameProperty(StringUtils.isEmpty(domainName)?".*":domainName,
					StringUtils.isEmpty(testCaseName)?".*":testCaseName, ".*", label);
		return Iterables.concat(allSameFieldUitrs1, allSameFieldUitrs2);
		

	}
	/**
	 * @return the neo4jSession
	 */
	public Session getNeo4jSession() {
		final Session neo4jSession2 = neo4jSession;
		if (neo4jSession2 == null) {
			throw new IllegalStateException("neo4j session");
		} else {
			return neo4jSession2;
		}
	}

	

	/**
	 * @param neo4jSession the neo4jSession to set
	 */
	public void setNeo4jSession(Session neo4jSession) {
		this.neo4jSession = neo4jSession;
	}

	/**
	 * @return the uitrRepo
	 */
	public UserInputTrainingRecordRepo getUitrRepo() {
		return uitrRepo;
	}

	/**
	 * @param uitrRepo the uitrRepo to set
	 */
	public void setUitrRepo(UserInputTrainingRecordRepo uitrRepo) {
		this.uitrRepo = uitrRepo;
	}

}
