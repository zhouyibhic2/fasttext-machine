/*******************************************************************************
 * ATE, Automation Test Engine
 *
 * Copyright 2016, Montreal PROT, or individual contributors as
 * indicated by the @author tags or express copyright attribution
 * statements applied by the authors.  All third-party contributions are
 * distributed under license by Montreal PROT.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package com.bigtester.tebloud.machine.fasttext.config;

import java.io.File;

import javax.inject.Inject;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.context.annotation.Lazy;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.servlet.config.annotation.PathMatchConfigurer;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurationSupport;

import com.bigtester.ate.tcg.model.ml.AteFastText;
import com.bigtester.ate.tcg.model.ml.FastTextTrainer;
import com.bigtester.ate.tcg.model.ml.IAteFastText;
import com.bigtester.ate.tcg.model.ml.IFastTextTrainer;
import com.bigtester.ate.tcg.model.ml.IMLPredictor;
import com.bigtester.ate.tcg.model.ml.ITrainingEntityPioRepo;
import com.bigtester.ate.tcg.model.ml.MLPredictor;
import com.bigtester.ate.tcg.model.ml.TrainingEntityPioRepo;
import com.bigtester.ate.tcg.service.ITebloudPredictor;
import com.bigtester.tebloud.machine.fasttext.config.RunningModeProperties.RunningMode;
import com.bigtester.tebloud.tcg.config.TebloudApplicationProperties;

// TODO: Auto-generated Javadoc
/**
 * The Class WebMvcConfig.
 */
@Configuration
@ComponentScan(basePackages = { "com.bigtester.ate.tcg", "com.bigtester.tebloud.machine" })
@EnableAspectJAutoProxy
@EnableConfigurationProperties(TebloudApplicationProperties.class)

public class WebMvcConfig  extends WebMvcConfigurationSupport{ // extends WebMvcConfigurationSupport {

	@Inject
	private RunningModeProperties runningModeProperties;

	@Inject
	private TebloudApplicationProperties appProperties;

	@Value("${tebloud.autName}")
	private String autName;


	  @Override
	  public void configurePathMatch(PathMatchConfigurer configurer) {
	    configurer.setUseSuffixPatternMatch(false);
	  }


	/**
	 * Ml trainer.
	 *
	 * @param autName
	 *            the aut name
	 * @param fastText
	 *            the fast text
	 * @return the ML trainer
	 */
	@Bean
	@Scope(value = WebApplicationContext.SCOPE_REQUEST, proxyMode = ScopedProxyMode.NO)
	@Lazy(value = true)
	public ITrainingEntityPioRepo trainingEntityPioRepo(String autName, IAteFastText fastText) {
		if (this.getRunningModeProperties().getRunningMode().equals(RunningMode.USECASE_APP.name())) {
			autName = this.getRunningModeProperties().getUsecaseAPPTrainingEventName();
		}
		return new TrainingEntityPioRepo(autName, fastText);
	}


//
//	@Bean
//	@Scope(value = WebApplicationContext.SCOPE_REQUEST, proxyMode = ScopedProxyMode.TARGET_CLASS)
//	@Lazy(value = true)
//	public TebloudUserZone requestUserWorkingZone() {
//		return new TebloudUserZone();
//	}





	@Bean
	// @Scope(value = WebApplicationContext.SCOPE_REQUEST, proxyMode =
	// ScopedProxyMode.NO)
	// @Scope(value = "prototype")
	//@Lazy(value = true)
	public IAteFastText fastTextPredictor() {

		if (this.getRunningModeProperties().getRunningMode().equals(RunningMode.USECASE_APP.name())) {
			autName = this.getRunningModeProperties().getUsecaseAPPTrainingEventName();
		}
		AteFastText jft = new AteFastText();
		// Train supervised model
		System.out.println(IFastTextTrainer.CURRENTDIR);
		String newFilePathName = IFastTextTrainer.CURRENTDIR + "src/test/resources/models/" + autName + "/supervised.model.bin";
		String defaultFilePathName = IFastTextTrainer.CURRENTDIR + "src/test/resources/models/supervised.model.bin";
		// Load model from file
		File modelFile = new File(newFilePathName);
		if (modelFile.exists())
			jft.loadModel(newFilePathName);
		else
			jft.loadModel(defaultFilePathName);
		return jft;
	}

	@Bean
	@Scope(value = WebApplicationContext.SCOPE_REQUEST, proxyMode = ScopedProxyMode.NO)
	@Lazy(value = true)
	public IFastTextTrainer fastTextTrainer(String autName) {
		if (this.getRunningModeProperties().getRunningMode().equals(RunningMode.USECASE_APP.name())) {
			autName = this.getRunningModeProperties().getUsecaseAPPTrainingEventName();
		}
		return new FastTextTrainer(autName, this.appProperties.getFasttext().getTrainingParams());
	}



	public RunningModeProperties getRunningModeProperties() {
		return runningModeProperties;
	}

	public void setRunningModeProperties(RunningModeProperties runningModeProperties) {
		this.runningModeProperties = runningModeProperties;
	}


}
