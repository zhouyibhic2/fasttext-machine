package com.bigtester.tebloud.machine.fasttext.ws;

import javax.inject.Inject;

import org.eclipse.jdt.annotation.Nullable;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.context.WebApplicationContext;

import com.bigtester.ate.tcg.model.ml.IAteFastText;
import com.bigtester.ate.tcg.model.ml.IFastTextTrainer;
import com.bigtester.ate.tcg.model.ml.IMLPredictor;
import com.bigtester.ate.tcg.model.ml.ITrainingEntityPioRepo;
import com.bigtester.ate.tcg.service.ITebloudPredictor;
import com.bigtester.ate.tcg.service.repository.PredictedFieldNameRepo;
import com.bigtester.ate.tcg.service.repository.ScreenJumperTrainingRecordRepo;
import com.bigtester.ate.tcg.service.repository.ScreenNodeRepo;
import com.bigtester.ate.tcg.service.repository.TestCaseRepo;
import com.bigtester.ate.tcg.service.repository.TestSuiteRepo;
import com.bigtester.ate.tcg.service.repository.UserInputTrainingRecordRepo;
import com.bigtester.ate.tcg.service.repository.UserInputValueRepo;
import com.bigtester.tebloud.machine.fasttext.config.RunningModeProperties;
import com.bigtester.tebloud.machine.fasttext.cuba.portal.security.TebloudPortalUserSessionSource;

import com.bigtester.tebloud.machine.fasttext.service.ScreenNodeCrud;
import com.bigtester.tebloud.machine.fasttext.service.TestCaseNodeCrud;
import com.bigtester.tebloud.machine.fasttext.service.TestSuiteNodeCrud;
import com.bigtester.tebloud.machine.fasttext.service.WindowsSystemFilePickerScreenNodeCrud;
import com.bigtester.tebloud.tcg.config.TebloudApplicationProperties;


/**
 * The Class GreetingController.
 */
public class BaseWsController {

	@Inject
	private RunningModeProperties runningModeProperties;

	@Autowired
	@Nullable
	private WebApplicationContext context;
	@Inject
	private TebloudApplicationProperties appProperties;



	@Autowired
	private BeanFactory beanFactory;

//	@Inject
//    private Metadata metadata;
//    @Inject
//    private DataManager dataManager;
//
//
//	@Inject
//	private UserSessionSource usSource;
//
//	public UserSessionSource getUsSource() {
//		return usSource;
//	}
//	public void setUsSource(UserSessionSource usSource) {
//		this.usSource = usSource;
//	}



//	/** The template. */
//	@Autowired
//	@Nullable
//	private transient Neo4jOperations template;

	/** The screen node repo. */
	@Autowired
	@Nullable
	private ScreenNodeRepo screenNodeRepo;

	/** The user input value repo. */
	@Autowired
	@Nullable
	private UserInputValueRepo userInputValueRepo;

	/** The predicted field name repo. */
	@Autowired
	@Nullable
	private PredictedFieldNameRepo predictedFieldNameRepo;

	/** The user input training record repo. */
	@Autowired
	@Nullable
	private UserInputTrainingRecordRepo userInputTrainingRecordRepo;

	/** The user click input training record repo. */
	@Autowired
	@Nullable
	private UserInputTrainingRecordRepo userClickInputTrainingRecordRepo;

	@Autowired
	@Nullable
	/** The action element training record repo. */
	private ScreenJumperTrainingRecordRepo actionElementTrainingRecordRepo;


	/** The test case repo. */
	@Autowired
	@Nullable
	private TestCaseRepo testCaseRepo;

	/** The screen node crud. */
	@Autowired
	@Nullable
	private ScreenNodeCrud screenNodeCrud;

	/** The windows system file picker screen node crud. */
	@Autowired
	@Nullable
	private WindowsSystemFilePickerScreenNodeCrud windowsSystemFilePickerScreenNodeCrud; //NOPMD



	/** The test case crud. */
	@Autowired
	@Nullable
	private TestCaseNodeCrud testCaseCrud;
	/** The test suite repo. */
	@Autowired
	@Nullable
	private TestSuiteRepo testSuiteRepo;

	/** The test suite crud. */
	@Autowired
	@Nullable
	private TestSuiteNodeCrud testSuiteCrud;

	@Autowired
	private IAteFastText tebloudFastText;

//	protected IMLPredictor createPredictorMachine(String autName) {
//		IAteFastText fastText = this.getBeanFactory().getBean(IAteFastText.class, autName);
//		fastText.unloadModel();
//		fastText.loadModel(autName);
//		return this.getBeanFactory().getBean(IMLPredictor.class, autName, fastText);
//	}
//	protected IScreenProbabilityCalculator createScreenProbabilityCalculator(String autName) {
//
//		return this.getBeanFactory().getBean(IScreenProbabilityCalculator.class, this.createPredictorService(autName));
//	}

	protected IFastTextTrainer createFastTextMachine(String autName) {
		//IAteFastText fastText = this.getBeanFactory().getBean(IAteFastText.class, autName);
		return this.getBeanFactory().getBean(IFastTextTrainer.class, autName);
	}

	protected ITrainingEntityPioRepo createTrainerMachine(String autName) {
		IAteFastText fastText = this.getBeanFactory().getBean(IAteFastText.class, autName);
		return this.getBeanFactory().getBean(ITrainingEntityPioRepo.class, autName, fastText);
	}


//	/**
//	 * @return the template
//	 */
//
//	public Neo4jOperations getTemplate() {
//		final Neo4jOperations template2 = template;
//		if (template2 == null) {
//			throw new IllegalStateException("template not initialized");
//		} else {
//			return template2;
//
//		}
//
//	}

//	/**
//	 * @param template
//	 *            the template to set
//	 */
//	public void setTemplate(Neo4jTemplate template) {
//		this.template = template;
//	}

	/**
	 * @return the screenNodeRepo
	 */
	public ScreenNodeRepo getScreenNodeRepo() {
		final ScreenNodeRepo screenNodeRepo2 = screenNodeRepo;
		if (screenNodeRepo2 == null) {
			throw new IllegalStateException("screenNodeRepo Initialization");
		} else {
			return screenNodeRepo2;
		}
	}

	/**
	 * @param screenNodeRepo
	 *            the screenNodeRepo to set
	 */
	public void setScreenNodeRepo(ScreenNodeRepo screenNodeRepo) {
		this.screenNodeRepo = screenNodeRepo;
	}


//	/**
//	 * @param template
//	 *            the template to set
//	 */
//	public void setTemplate(Neo4jOperations template) {
//		this.template = template;
//	}

	/**
	 * @return the testCaseRepo
	 */
	public TestCaseRepo getTestCaseRepo() {
		final TestCaseRepo testCaseRepo2 = testCaseRepo;
		if (testCaseRepo2 == null) {
			throw new IllegalStateException("test case repo");
		} else {
			return testCaseRepo2;

		}
	}

	/**
	 * @param testCaseRepo
	 *            the testCaseRepo to set
	 */
	public void setTestCaseRepo(TestCaseRepo testCaseRepo) {
		this.testCaseRepo = testCaseRepo;
	}



	/**
	 * @return the testSuiteRepo
	 */
	public TestSuiteRepo getTestSuiteRepo() {
		final TestSuiteRepo testSuiteRepo2 = testSuiteRepo;
		if (testSuiteRepo2 == null) {
			throw new IllegalStateException("testsuiterepo");
		} else {
			return testSuiteRepo2;
		}
	}

	/**
	 * @param testSuiteRepo
	 *            the testSuiteRepo to set
	 */
	public void setTestSuiteRepo(TestSuiteRepo testSuiteRepo) {
		this.testSuiteRepo = testSuiteRepo;
	}

	/**
	 * @return the userInputValueRepo
	 */
	public UserInputValueRepo getUserInputValueRepo() {
		final UserInputValueRepo userInputValueRepo2 = userInputValueRepo;
		if (userInputValueRepo2 == null) {
			throw new IllegalStateException("userinputvaluerepo");

		} else {
			return userInputValueRepo2;
		}
	}

	/**
	 * @param userInputValueRepo
	 *            the userInputValueRepo to set
	 */
	public void setUserInputValueRepo(UserInputValueRepo userInputValueRepo) {
		this.userInputValueRepo = userInputValueRepo;
	}

	/**
	 * @return the predictedFieldNameRepo
	 */
	public PredictedFieldNameRepo getPredictedFieldNameRepo() {
		final PredictedFieldNameRepo predictedFieldNameRepo2 = predictedFieldNameRepo;
		if (predictedFieldNameRepo2 == null) {
			throw new IllegalStateException("PredictedFieldnameRepo");
		} else {
			return predictedFieldNameRepo2;

		}
	}

	/**
	 * @param predictedFieldNameRepo
	 *            the predictedFieldNameRepo to set
	 */
	public void setPredictedFieldNameRepo(
			PredictedFieldNameRepo predictedFieldNameRepo) {
		this.predictedFieldNameRepo = predictedFieldNameRepo;
	}

	/**
	 * @return the userInputTrainingRecordRepo
	 */
	public UserInputTrainingRecordRepo getUserInputTrainingRecordRepo() {
		final UserInputTrainingRecordRepo userInputTrainingRecordRepo2 = userInputTrainingRecordRepo;
		if (userInputTrainingRecordRepo2 == null) {
			throw new IllegalStateException("userinputtrainingrecordrepo");
		} else {
			return userInputTrainingRecordRepo2;
		}
	}

	/**
	 * @param userInputTrainingRecordRepo
	 *            the userInputTrainingRecordRepo to set
	 */
	public void setUserInputTrainingRecordRepo(
			UserInputTrainingRecordRepo userInputTrainingRecordRepo) {
		this.userInputTrainingRecordRepo = userInputTrainingRecordRepo;
	}

	/**
	 * @return the screenNodeCrud
	 */
	public ScreenNodeCrud getScreenNodeCrud() {
		final ScreenNodeCrud screenNodeCrud2 = screenNodeCrud;
		if (screenNodeCrud2 != null) {
			return screenNodeCrud2;
		} else {
			throw new IllegalStateException("screenNodeCrud");
		}
	}

	/**
	 * @param screenNodeCrud
	 *            the screenNodeCrud to set
	 */
	public void setScreenNodeCrud(ScreenNodeCrud screenNodeCrud) {
		this.screenNodeCrud = screenNodeCrud;
	}


	/**
	 * @return the actionElementTrainingRecordRepo
	 */
	public ScreenJumperTrainingRecordRepo getActionElementTrainingRecordRepo() {
		final ScreenJumperTrainingRecordRepo actionElementTrainingRecordRepo2 = actionElementTrainingRecordRepo;
		if (actionElementTrainingRecordRepo2 != null) {
			return actionElementTrainingRecordRepo2;
		} else {
			throw new IllegalStateException("actionelementtrainingrecordrepo");
		}
	}

	/**
	 * @param actionElementTrainingRecordRepo
	 *            the actionElementTrainingRecordRepo to set
	 */
	public void setActionElementTrainingRecordRepo(
			ScreenJumperTrainingRecordRepo actionElementTrainingRecordRepo) {
		this.actionElementTrainingRecordRepo = actionElementTrainingRecordRepo;
	}

	/**
	 * @return the testSuiteCrud
	 */
	public TestSuiteNodeCrud getTestSuiteCrud() {
		final TestSuiteNodeCrud testSuiteCrud2 = testSuiteCrud;
		if (testSuiteCrud2 != null) {
			return testSuiteCrud2;
		} else {
			throw new IllegalStateException("test suite crud");
		}
	}

	/**
	 * @param testSuiteCrud
	 *            the testSuiteCrud to set
	 */
	public void setTestSuiteCrud(TestSuiteNodeCrud testSuiteCrud) {
		this.testSuiteCrud = testSuiteCrud;
	}

	/**
	 * @return the testCaseCrud
	 */
	public TestCaseNodeCrud getTestCaseCrud() {
		final TestCaseNodeCrud testCaseCrud2 = testCaseCrud;
		if (testCaseCrud2 != null) {
			return testCaseCrud2;
		} else {
			throw new IllegalStateException("test case crud");
		}
	}

	/**
	 * @param testCaseCrud
	 *            the testCaseCrud to set
	 */
	public void setTestCaseCrud(TestCaseNodeCrud testCaseCrud) {
		this.testCaseCrud = testCaseCrud;
	}

	/**
	 * @return the userClickInputTrainingRecordRepo
	 */
	public UserInputTrainingRecordRepo getUserClickInputTrainingRecordRepo() {
		final UserInputTrainingRecordRepo userClickInputTrainingRecordRepo2 = userClickInputTrainingRecordRepo;
		if (userClickInputTrainingRecordRepo2 != null) {
			return userClickInputTrainingRecordRepo2;
		} else {
			throw new IllegalStateException("userclickinputtrainingrecordrepo");
		}
	}

	/**
	 * @param userClickInputTrainingRecordRepo the userClickInputTrainingRecordRepo to set
	 */
	public void setUserClickInputTrainingRecordRepo(
			UserInputTrainingRecordRepo userClickInputTrainingRecordRepo) {
		this.userClickInputTrainingRecordRepo = userClickInputTrainingRecordRepo;
	}

	/**
	 * @return the windowsSystemFilePickerScreenNodeCrud
	 */
	public WindowsSystemFilePickerScreenNodeCrud getWindowsSystemFilePickerScreenNodeCrud() {
		final WindowsSystemFilePickerScreenNodeCrud windowsSystemFilePickerScreenNodeCrud2 = windowsSystemFilePickerScreenNodeCrud;//NOPMD
		if (windowsSystemFilePickerScreenNodeCrud2 != null) {
			return windowsSystemFilePickerScreenNodeCrud2;
		} else {
			throw new IllegalStateException("windowssystemfilepickerScreenNodeCrud");
		}
	}

	/**
	 * @param windowsSystemFilePickerScreenNodeCrud the windowsSystemFilePickerScreenNodeCrud to set
	 */
	public void setWindowsSystemFilePickerScreenNodeCrud(
			WindowsSystemFilePickerScreenNodeCrud windowsSystemFilePickerScreenNodeCrud) {//NOPMD
		this.windowsSystemFilePickerScreenNodeCrud = windowsSystemFilePickerScreenNodeCrud;
	}

	/**
	 * @return the pioPredictor
	 */
	public ITebloudPredictor createPredictorService(String autName) {
		IAteFastText fastText = this.getBeanFactory().getBean(IAteFastText.class, autName);
		IMLPredictor appP = this.getBeanFactory().getBean(IMLPredictor.class, autName, fastText);
		return this.getBeanFactory().getBean(ITebloudPredictor.class, appP);
	}


//	public TebloudUser fetchTebloudUserWithoutZoneInfo(String userSessionId) {
//		TebloudUser user = (TebloudUser) usSource.getUserSession().getUser();
//		if (usSource instanceof TebloudPortalUserSessionSource ) {
//			UserSession userS = ((TebloudPortalUserSessionSource) usSource).getLoggedUserSession(userSessionId).orElse(usSource.getUserSession());
//			user = (TebloudUser) userS.getUser();
//		}
//		Objects.requireNonNull(user);
//
//	    user = dataManager.reload(user, View.LOCAL);
//        return user;
//	}
//	public boolean isValidPortalSession() {
//		return usSource.checkCurrentUserSession();
//	}
//
//	public boolean isLoggedInSession(String sessionid) {
//		return isValidPortalSession() && (getUsSource() instanceof TebloudPortalUserSessionSource ) && ((TebloudPortalUserSessionSource)getUsSource()).getLoggedUserSession(sessionid).isPresent();
//
//	}
//
//
//
//
//		TebloudUser user = (TebloudUser)  ((TebloudPortalUserSessionSource) usSource).getLoggedUserSession(userSessionId).orElseThrow(()->new IllegalStateException("User not logged in")).getUser();
//
//
//
//		Objects.requireNonNull(user);
//
//		View view = new View(TebloudUser.class)
//		        .addProperty("tebloudUserZone");
//
//        user = dataManager.reload(user, view);
//        long userZoneCount = user.getTebloudUserZone().stream().filter(tuz->tuz.getZoneDomainName().equals(zoneDomainName)).count();
//        TebloudUserZone zone;
//		if (userZoneCount==1) {
//			zone = user.getTebloudUserZone().stream().filter(tuz->tuz.getZoneDomainName().equals(zoneDomainName)).collect(Collectors.toList()).get(0);
//		} else if(userZoneCount==0) {
//			zone = metadata.create(TebloudUserZone.class);
//
//			if (zone.getTebloudUsers()==null) {
//				List<TebloudUser> users = new ArrayList<TebloudUser>();
//				users.add(user);
//				zone.setTebloudUsers(users);
//			} else {
//				zone.getTebloudUsers().add(user);
//			}
//			zone.setZoneDomainName(zoneDomainName);
//
//
//		    //TODO tacle the 2 data sources transaction atomic issue. roll back failed if the neo4j
//			//transaction failed, for example domain model not in the sessionfactory
//
//			user.getTebloudUserZone().add(zone);
//
//		    dataManager.commit(zone);
//		    dataManager.commit(user);
//
//			////////////////////above is for testing code////
//
//		} else {
//			throw new IllegalStateException("number of tebloud user zones is incorrect!");
//		}
//		return zone;

	/**
	 * @return the beanFactory
	 */
	public BeanFactory getBeanFactory() {
		return beanFactory;
	}

	/**
	 * @param beanFactory the beanFactory to set
	 */
	public void setBeanFactory(BeanFactory beanFactory) {
		this.beanFactory = beanFactory;
	}


	/**
	 * @return the context
	 */
	public WebApplicationContext getContext() {
		final WebApplicationContext context2 = context;
		if (context2 == null) {
			throw new IllegalStateException(
					"Web Application Context Not Initialized!");
		} else {
			return context2;
		}
	}

	/**
	 * @param context
	 *            the context to set
	 */
	public void setContext(WebApplicationContext context) {
		this.context = context;
	}

	/**
	 * @return the runningModeProperties
	 */
	public RunningModeProperties getRunningModeProperties() {
		return runningModeProperties;
	}
	/**
	 * @param runningModeProperties the trainingModeProperties to set
	 */
	public void setRunningModeProperties(RunningModeProperties runningModeProperties) {
		this.runningModeProperties = runningModeProperties;
	}
	/**
	 * @return the tebloudFastText
	 */
	public IAteFastText getTebloudFastText() {
		return tebloudFastText;
	}
	/**
	 * @param tebloudFastText the tebloudFastText to set
	 */
	public void setTebloudFastText(IAteFastText tebloudFastText) {
		this.tebloudFastText = tebloudFastText;
	}

	/**
	 * @return the appProperties
	 */
	public TebloudApplicationProperties getAppProperties() {
		return appProperties;
	}

	/**
	 * @param appProperties the appProperties to set
	 */
	public void setAppProperties(TebloudApplicationProperties appProperties) {
		this.appProperties = appProperties;
	}


}
