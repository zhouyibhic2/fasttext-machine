/*******************************************************************************
 * ATE, Automation Test Engine
 *
 * Copyright 2016, Montreal PROT, or individual contributors as
 * indicated by the @author tags or express copyright attribution
 * statements applied by the authors.  All third-party contributions are
 * distributed under license by Montreal PROT.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package com.bigtester.tebloud.machine.fasttext.ws;//NOPMD

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ExecutionException;
import java.util.stream.Collectors;

import org.apache.commons.io.FileUtils;
import org.eclipse.jdt.annotation.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.WebApplicationContext;

import com.bigtester.ate.tcg.model.ml.FastTextTrainer;
import com.bigtester.ate.tcg.model.ml.IAteFastText;
import com.bigtester.ate.tcg.model.ml.IFastTextTrainer;
import com.bigtester.ate.tcg.model.ml.TrainingEntityPioRepo;
import com.bigtester.tebloud.machine.fasttext.config.TrainingParams;


/**
 * The Class GreetingController.
 */
@RestController
@RequestMapping(value = "/fasttext")
public class FastTextController extends BaseWsController {





	@Autowired
	@Nullable
	private WebApplicationContext context;






	@CrossOrigin
	@RequestMapping(value = "/train/{autName:.+}", method = RequestMethod.POST)

	public String train(@PathVariable String autName,TrainingParams query){
		this.getAppProperties().getFasttext().setTrainingParams(query);
		//doesn't know if it is done correctly
		try {
			return this.createFastTextMachine(autName).trainFastTextModelForAppEngine(this.createTrainerMachine(autName).queryAllUitrTrainingEntitiesForAut());
		} catch (ExecutionException | InterruptedException | IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return "failed" + ": " + e.getMessage();
		}

	}
	@CrossOrigin
	@RequestMapping(value = "/predict/{autName:.+}", method = RequestMethod.POST)

	public IAteFastText.ProbLabel predictProb(@RequestBody Map<String, String> query, @PathVariable String autName) throws IOException{

		List<String> vecs = FileUtils.readLines(new File(IFastTextTrainer.getVecFilePathName(autName)));
		String text = query.values().iterator().next();
		 Set<String> unknowns = Arrays.stream(text.split("\\s+")).filter(word->{
			 return vecs.stream().filter(vec->vec.contains(word)).count()==0;
		 }).collect(Collectors.toSet());
		 for (Iterator<String> itr= unknowns.iterator(); itr.hasNext();) {
			 text = text.replaceAll(itr.next(), IFastTextTrainer.FAST_TEXT_TRAINING_UNKNOWN_WORD);
		 }
		return this.getTebloudFastText().predictProba(text);

	}


	/**
	 * @return the context
	 */
	public WebApplicationContext getContext() {
		final WebApplicationContext context2 = context;
		if (context2 == null) {
			throw new IllegalStateException(
					"Web Application Context Not Initialized!");
		} else {
			return context2;
		}
	}

	/**
	 * @param context
	 *            the context to set
	 */
	public void setContext(WebApplicationContext context) {
		this.context = context;
	}



}
