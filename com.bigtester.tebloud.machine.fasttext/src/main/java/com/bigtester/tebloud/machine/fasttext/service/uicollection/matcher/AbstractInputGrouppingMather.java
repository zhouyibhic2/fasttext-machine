/*******************************************************************************
 * ATE, Automation Test Engine
 *
 * Copyright 2017, Montreal PROT, or individual contributors as
 * indicated by the @author tags or express copyright attribution
 * statements applied by the authors.  All third-party contributions are
 * distributed under license by Montreal PROT.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package com.bigtester.tebloud.machine.fasttext.service.uicollection.matcher;

import static org.joox.JOOX.$;
import static org.joox.JOOX.attr;

import org.joox.FastFilter;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import org.joox.Context;
import org.joox.Filter;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

import com.bigtester.ate.tcg.controller.WebFormUserInputsCollectorHtmlTerms;
import com.bigtester.ate.tcg.controller.WebFormUserInputsCollectorHtmlTerms.HtmlInputType;
import com.bigtester.ate.tcg.controller.WebFormUserInputsCollectorHtmlTerms.UserCollectableHtmlTag;
import com.google.common.collect.Iterables;

// TODO: Auto-generated Javadoc
/**
 * This class AbstractInputGrouppingMather defines ....
 * @author Peidong Hu
 *
 */
abstract public class AbstractInputGrouppingMather {
	private final Set<UserCollectableHtmlTag> userCollectableHtmlTags = new HashSet<UserCollectableHtmlTag>();
	private final Optional<HtmlInputType> htmlInputType;
	
	public AbstractInputGrouppingMather(UserCollectableHtmlTag userCollectableHtmlTag, Optional<HtmlInputType> htmlInputType) {
		this.userCollectableHtmlTags.add(userCollectableHtmlTag);
		this.htmlInputType = htmlInputType;
	}
	
	public AbstractInputGrouppingMather(Set<UserCollectableHtmlTag> userCollectableHtmlTags, Optional<HtmlInputType> htmlInputType) {
		this.userCollectableHtmlTags.addAll(userCollectableHtmlTags);
		this.htmlInputType = htmlInputType;
	}
	
	public Optional<Node> matchParentNode(String parentNodeTagName, Node childNode) {
		Optional<Node> retVal = Optional.empty();
		
		List<Element> parents = new ArrayList<Element>();
		if (childNode.getParentNode().getNodeName().equals(parentNodeTagName)) {
			parents.add((Element) childNode.getParentNode());
		} else {
			parents = $(childNode)
				.parentsUntil(parentNodeTagName).parent().get();
		}
		
		if (!parents.isEmpty() && $(parents).find(parentNodeTagName).isEmpty() && !Iterables
				.get(parents,
						parents.size() - 1)
				.getNodeName().equalsIgnoreCase("html")) {
			
			retVal = Optional.of(parents.get(parents.size() - 1));
		}
		return retVal;
	}
	
	public Optional<Node> matchParentNode(List<Node> childNodes, String attriName, String valueOfNameAttr, boolean attributePrefixValue) {
		//attriName = "value";
		//valueOfNameAttr = "ex_contact_reference_yes";
		if (childNodes.size()==1) return Optional.of(childNodes.get(0));

		//String matchStr = this.getUserCollectableHtmlTag() + "["+attriName+"='" + valueOfNameAttr + "']";

		
		Set<UserCollectableHtmlTag> allPossibleTags = new HashSet<UserCollectableHtmlTag>(Arrays.asList(UserCollectableHtmlTag.values()));
		List<Element> allVisiableInputsUnderDirectParent = new ArrayList<Element>();
		allPossibleTags.forEach((ucht -> {
			allVisiableInputsUnderDirectParent.addAll($(childNodes.get(0).getParentNode())
					.find(ucht.toString()).filter(new FastFilter() {
						@Override
						public boolean filter(Context context) {

							return context.element().getNodeName()
									.equalsIgnoreCase(ucht.toString())
									&& (context
											.element()
											.getAttribute(
													WebFormUserInputsCollectorHtmlTerms.ATE_INVISIBLE_ATTR_NAME) == null
											|| context
													.element()
													.getAttribute(
															WebFormUserInputsCollectorHtmlTerms.ATE_INVISIBLE_ATTR_NAME)
													.isEmpty() 
											|| !context
												.element()
												.getAttribute(
														WebFormUserInputsCollectorHtmlTerms.ATE_INVISIBLE_ATTR_NAME)
												.equalsIgnoreCase(
														WebFormUserInputsCollectorHtmlTerms.ATE_YES_ATTR_VALUE));
						}
					}).get());
		}));
		if (childNodes.size()< allVisiableInputsUnderDirectParent.size()) return Optional.empty();
		
		List<Element> parents = new ArrayList<Element>();
		List<Element> directParentinputs = new ArrayList<Element>();
 
		getUserCollectableHtmlTags().forEach(ucht->{
			if (attributePrefixValue) {
				directParentinputs.addAll($(childNodes.get(0).getParentNode()).find(ucht.toString()).filter(new FastFilter(){

						@Override
						public boolean filter(Context context) {
							
							return $(context.element()).attr(attriName).startsWith(valueOfNameAttr);
						}
					
				}).get());
			} else {
				directParentinputs.addAll($(childNodes.get(0).getParentNode()).find(ucht.toString()).filter(attr(attriName, valueOfNameAttr)).get());
			}
		});
		
		
		
		if (directParentinputs.size() == childNodes.size() && allVisiableInputsUnderDirectParent.size() == childNodes.size()) {
			
				//direct parent has the matched input
				parents.add((Element)childNodes.get(0).getParentNode());
			

		} else {
			parents = $(childNodes.get(0))
				.parentsUntil(new Filter(){
					@Override
			        public boolean filter(Context context) {
						//System.out.println(matchStr+"");
						List<Element> inputs = new ArrayList<Element>();
						getUserCollectableHtmlTags().forEach(ucht->{
							if (attributePrefixValue) {
								inputs.addAll($(context.element()).find(ucht.toString()).filter(new FastFilter(){

									@Override
									public boolean filter(Context context) {
										String value = $(context.element()).attr(attriName);
										return value == null? false : value.startsWith(valueOfNameAttr);
									}
								
							}).get());								
							} else {
								inputs.addAll($(context.element()).find(ucht.toString()).filter(attr(attriName, valueOfNameAttr)).get());
							}
						});
						List<Element> possibleInputs = new ArrayList<Element>();
						allPossibleTags.forEach((ucht -> {
							possibleInputs.addAll($(context.element())
									.find(ucht.toString()).filter(new FastFilter() {
										@Override
										public boolean filter(Context context) {

											return context.element().getNodeName()
													.equalsIgnoreCase(ucht.toString())
													&& (context
															.element()
															.getAttribute(
																	WebFormUserInputsCollectorHtmlTerms.ATE_INVISIBLE_ATTR_NAME) == null
															|| context
																	.element()
																	.getAttribute(
																			WebFormUserInputsCollectorHtmlTerms.ATE_INVISIBLE_ATTR_NAME)
																	.isEmpty() 
															|| !context
																.element()
																.getAttribute(
																		WebFormUserInputsCollectorHtmlTerms.ATE_INVISIBLE_ATTR_NAME)
																.equalsIgnoreCase(
																		WebFormUserInputsCollectorHtmlTerms.ATE_YES_ATTR_VALUE));
										}
									}).get());
						}));

						return inputs.size() == childNodes.size() && possibleInputs.size() == childNodes.size();
					}
				}).parent().get();
//			if(Iterables
//			.get(parents,
//					parents.size() - 1)
//			.getNodeName().equalsIgnoreCase("html")) {
//				//Not found
//				parents.clear();
//			}
		}
//		if (!parents.isEmpty() && $(parents).find(matchStr).isEmpty() && !Iterables
//				.get(parents,
//						parents.size() - 1)
//				.getNodeName().equalsIgnoreCase("html")) {
//			
		//	retVal = parents.get(parents.size() - 1);
//		}
		Optional<Node> retVal =  Optional.empty();
		if (!parents.isEmpty() && !Iterables
		.get(parents,
				parents.size() - 1)
		.getNodeName().equalsIgnoreCase("html")) {

			retVal = Optional.of(parents.get(parents.size() - 1));
			if ($(retVal.get()).find("label").isEmpty()) {
				if ($(retVal.get().getParentNode()).find("label").isNotEmpty()) {
					retVal = Optional.of(retVal.get().getParentNode());
				}
			}
		} 
		return 	retVal;
	}
/**
 * @return the userCollectableHtmlTag
 */
public Set<UserCollectableHtmlTag> getUserCollectableHtmlTags() {
	return userCollectableHtmlTags;
}
/**
 * @return the htmlInputType
 */
public Optional<HtmlInputType> getHtmlInputType() {
	return htmlInputType;
}
}
