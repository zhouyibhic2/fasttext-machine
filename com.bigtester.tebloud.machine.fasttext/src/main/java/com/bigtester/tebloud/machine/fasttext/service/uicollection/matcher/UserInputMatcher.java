/*******************************************************************************
 * ATE, Automation Test Engine
 *
 * Copyright 2016, Montreal PROT, or individual contributors as
 * indicated by the @author tags or express copyright attribution
 * statements applied by the authors.  All third-party contributions are
 * distributed under license by Montreal PROT.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package com.bigtester.tebloud.machine.fasttext.service.uicollection.matcher;
 

import com.bigtester.ate.tcg.controller.WebFormUserInputsCollectorHtmlTerms.HtmlInputType; 
import com.bigtester.ate.tcg.controller.WebFormUserInputsCollectorHtmlTerms.UserCollectableHtmlTag;
import com.bigtester.ate.tcg.model.UserInputDom;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

// TODO: Auto-generated Javadoc
/**
 * This class OutOfFormInputListMatcher defines ....
 * 
 * @author Peidong Hu
 *
 */
public class UserInputMatcher extends
		AbstractInputGrouppingMather implements
		IUIGrouppedByAttributeMatcher, IUIGrouppedByParentTagMatcher {
	final private Optional<String> matchingAttributeName;
	final private Optional<String> matchingHtmlTagName;
	final private boolean attrValuePrefixFilterred;
	final static private Set<String> matchingAttributeValuePrefixSeperators = new HashSet<String>(Arrays.asList("_"));
	/**
	 * 
	 */
	public UserInputMatcher(
			IUIGrouppedByAttributeMatcher.SupportedMatchingInputAttribute matchingAttributeName,
			UserCollectableHtmlTag userCollectableHtmlTag,
			Optional<HtmlInputType> htmlInputType) {
		super(userCollectableHtmlTag, htmlInputType);
		this.matchingAttributeName = Optional.of(matchingAttributeName
				.toString());
		this.matchingHtmlTagName = Optional.empty();
		this.attrValuePrefixFilterred = false;
	}

	public UserInputMatcher(
			IUIGrouppedByParentTagMatcher.SupportedMatchingParentHtmlTag matchingTagName,
			UserCollectableHtmlTag userCollectableHtmlTag,
			Optional<HtmlInputType> htmlInputType) {
		super(userCollectableHtmlTag, htmlInputType);
		this.matchingHtmlTagName = Optional.of(matchingTagName.toString());
		this.matchingAttributeName = Optional.empty();
		this.attrValuePrefixFilterred = false;
	}

	public UserInputMatcher(
			IUIGrouppedByParentTagMatcher.SupportedMatchingParentHtmlTag matchingTagName,
			UserCollectableHtmlTag userCollectableHtmlTag,
			Optional<HtmlInputType> htmlInputType,
			boolean attrValuePrefixFilterred) {
		super(userCollectableHtmlTag, htmlInputType);
		this.matchingHtmlTagName = Optional.of(matchingTagName.toString());
		this.matchingAttributeName = Optional.empty();
		this.attrValuePrefixFilterred = attrValuePrefixFilterred;
	}
	
	public UserInputMatcher(
			IUIGrouppedByAttributeMatcher.SupportedMatchingInputAttribute matchingAttributeName,
			Set<UserCollectableHtmlTag> userCollectableHtmlTags,
			boolean attrValuePrefixFilterred) {
		super(userCollectableHtmlTags, Optional.empty());
		this.matchingHtmlTagName = Optional.empty();
		this.matchingAttributeName = Optional.of(matchingAttributeName.toString());
		this.attrValuePrefixFilterred = attrValuePrefixFilterred;
	}
	

	
	/**
	 * @return the matchingAttributeName
	 */
	public Optional<String> getMatchingAttributeName() {
		return matchingAttributeName;
	}

	/**
	 * @return the matchingHtmlTagName
	 */
	public Optional<String> getMatchingHtmlTagName() {
		return matchingHtmlTagName;
	}

	/**
	 * Match pattern.
	 *
	 * @param doc the doc
	 * @param matchInputNodes the match input nodes
	 * @return the map
	 */
	@Override
	public Map<UserInputDom, List<UserInputDom>> matchPattern(Document doc,
			List<UserInputDom> matchInputNodes) { 
		List<UserInputDom> radioNodes = matchInputNodes
				.stream()
				.filter(inputNode -> {
					if (this.getUserCollectableHtmlTags().contains(inputNode.getCoreNodeInputType().getMainType())
							) {
						if (this.getHtmlInputType().isPresent()) {
							if (inputNode.getCoreNodeInputType().getSubType()
									.isPresent()) {
								return this.getHtmlInputType().get().equals(
										inputNode.getCoreNodeInputType()
												.getSubType().get());
							} else
								return false;

						} else {
							return true;
						}

					} else {
						return false;
					}
				}).collect(Collectors.toList());
		Map<UserInputDom, List<UserInputDom>> retVal = new ConcurrentHashMap<UserInputDom, List<UserInputDom>>();
		if (this.getMatchingHtmlTagName().isPresent()) {
			radioNodes.forEach(radio->{
				
				Optional<Node> tmpFieldSet = this.matchParentNode(this.getMatchingHtmlTagName().get(), radio.getDomNodePointer());
				tmpFieldSet.ifPresent(fieldS-> {
					List<UserInputDom> matchedFielsetUIDs = retVal.keySet().stream().filter(fieldsetN->{
						return fieldS == fieldsetN.getDomNodePointer();//NOPMD
					}).collect(Collectors.toList());
					//not in the fieldset Node map yet.
					if (matchedFielsetUIDs.size()==0) {
							List<UserInputDom> radios = new ArrayList<UserInputDom>();
							radios.add(radio);
							retVal.put(new UserInputDom(fieldS), radios);
					} else if (matchedFielsetUIDs.size()==1) {
						//fieldset already in map, add the child radio too
						if (!retVal.get(matchedFielsetUIDs.get(0)).contains(radio)) {
							retVal.get(matchedFielsetUIDs.get(0)).add(radio);
						}
					} else {
						throw new IllegalStateException("wrong map of " + getMatchingHtmlTagName().toString());
					}
				});
				});
		} else if (this.getMatchingAttributeName().isPresent()) {
			Map<String, List<UserInputDom>> tmpRetVal = radioNodes.stream().collect(Collectors.groupingBy(radioUid->{
				Element radioE = (Element) radioUid.getDomNodePointer();
				String nameAttrValue = radioE.getAttribute(this.getMatchingAttributeName().get());
				if (this.isAttrValuePrefixFilterred()) {
					for (Iterator<String> itr = getMatchingAttributeValuePrefixSeperators().iterator(); itr.hasNext(); ) {
						String sepeator = itr.next();
						String nameAttrValueSeperated = nameAttrValue.substring(0, nameAttrValue.lastIndexOf(sepeator)>0?nameAttrValue.lastIndexOf(sepeator):nameAttrValue.length());
						if (nameAttrValueSeperated.length()<nameAttrValue.length()) {
							nameAttrValue = nameAttrValueSeperated; 
							break;
						}
					}
				}
				return nameAttrValue;
				
			}));
			
			tmpRetVal.forEach((key, uidEntry)->{
				if (uidEntry.size()>1) {
					Optional<Node> parent = matchParentNode(uidEntry.stream().map(uid->uid.getDomNodePointer()).collect(Collectors.toList()), this.getMatchingAttributeName().get(), key, this.isAttrValuePrefixFilterred());
//					List<UserInputDom> matchedFielsetUIDs = retVal.keySet().stream().filter(fieldsetN->{
//						return key.equals(((Element)fieldsetN.getDomNodePointer()).getAttribute(this.getMatchingAttributeName().get()));//NOPMD
//					}).collect(Collectors.toList());
					//not in the return Node map yet.
//					if (matchedFielsetUIDs.size()==0) {
					if (parent.isPresent()) { retVal.put(new UserInputDom(parent.get()), uidEntry); };
//					} 
				}
				
			});
		}
		
		Map<UserInputDom, List<UserInputDom>>  singleChildMatchedParents = retVal.entrySet().stream().filter(entry->entry.getValue().size()==1).collect(Collectors.toMap(Entry::getKey, Entry::getValue));
		retVal.entrySet().removeIf(entry->entry.getValue().size()==1);
		singleChildMatchedParents.entrySet().forEach(entry->{
			retVal.put(entry.getValue().get(0), entry.getValue());	
		});
		
		return retVal;
	}

	/**
	 * @return the matchingattributevalueprefixseperator
	 */
	public static Set<String> getMatchingAttributeValuePrefixSeperators() {
		return matchingAttributeValuePrefixSeperators;
	}

	/**
	 * @return the attrValuePrefixFilterred
	 */
	public boolean isAttrValuePrefixFilterred() {
		return attrValuePrefixFilterred;
	}

	

	

	

}
