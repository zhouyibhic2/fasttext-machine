package com.bigtester.tebloud.machine.fasttext.ws;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.springframework.web.servlet.HandlerMapping;

import com.bigtester.tebloud.machine.fasttext.cuba.portal.security.TebloudPortalUserSessionSource;

@Aspect
@Component
public class RequestMappingControllerAcpect {

	//@Around("@annotation(org.springframework.web.bind.annotation.RequestMapping) && (within(com.bigtester.tebloud.tcg.ws..*))")
    public Object populateUserAndZone(ProceedingJoinPoint joinPoint) throws Throwable {
		HttpServletRequest request = ((ServletRequestAttributes)RequestContextHolder.getRequestAttributes()).getRequest();
		@SuppressWarnings("rawtypes")
		Map pathVariables = (Map) request.getAttribute(HandlerMapping.URI_TEMPLATE_VARIABLES_ATTRIBUTE);
	    String domainName = (String) pathVariables.get("domainName");
	    if (!StringUtils.isEmpty(domainName) && !domainName.equals("*") ) {

			Long userId = 0l;
			try {
				//userId = PortalUtil.getBasicAuthUserId(request);

			}
			catch (Exception e) {
				throw new IllegalStateException("user id not authenticated");
			}


			BaseWsController requestMappingController = (BaseWsController) joinPoint.getTarget();

		//if (!requestMappingController.isLoggedInSession(sessionid)) {
		//	throw new IllegalStateException("User not logged in");
		//}

	    	//requestMappingController.setRequestUserWorkingZone(requestMappingController.createTebloudUserZone(userId, domainName));
	    	//requestMappingController.setRequestUser(requestMappingController.fetchTebloudUserWithoutZoneInfo(sessionid));
	    }
        final Object proceed = joinPoint.proceed();

        return proceed;
    }
}
