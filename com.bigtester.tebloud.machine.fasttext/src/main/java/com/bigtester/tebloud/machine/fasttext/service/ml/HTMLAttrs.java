/*******************************************************************************
 * ATE, Automation Test Engine
 *
 * Copyright 2016, Montreal PROT, or individual contributors as
 * indicated by the @author tags or express copyright attribution
 * statements applied by the authors.  All third-party contributions are
 * distributed under license by Montreal PROT.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package com.bigtester.tebloud.machine.fasttext.service.ml;

/*use this js to collect the html tags from w3.org web page
https://www.w3.org/TR/2012/WD-html-markup-20121025/Overview.html#toc

var s = document.createElement('script');
s.setAttribute('src', 'http://code.jquery.com/jquery-latest.min.js');
document.getElementsByTagName('head')[0].appendChild(s);

var elements = $(".element");
$.each(elements, function(index, val) {
   console.log('"'+$(val).text() +'",');
});

*/
/**
 * This class HTMLAttrs defines ....
 * @author Peidong Hu
 *
 */
final public class HTMLAttrs {
	public static String[] attrList = {
		
	};

	/**
	 * 
	 */
	private HTMLAttrs() {
		// TODO Auto-generated constructor stub
	}

}
