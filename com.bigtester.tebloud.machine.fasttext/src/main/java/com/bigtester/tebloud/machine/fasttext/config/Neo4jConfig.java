/*******************************************************************************
 * ATE, Automation Test Engine
 *
 * Copyright 2015, Montreal PROT, or individual contributors as
 * indicated by the @author tags or express copyright attribution
 * statements applied by the authors.  All third-party contributions are
 * distributed under license by Montreal PROT.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package com.bigtester.tebloud.machine.fasttext.config;

import org.neo4j.ogm.session.SessionFactory;
import org.springframework.beans.factory.annotation.Value;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.data.neo4j.repository.config.EnableNeo4jRepositories;
//import org.springframework.data.neo4j.config.Neo4jConfiguration;
import org.springframework.data.neo4j.transaction.Neo4jTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

// TODO: Auto-generated Javadoc
/**
 * This class Neo4jConfig defines ....
 * @author Peidong Hu
 *
 */
@Configuration
@EnableNeo4jRepositories(basePackages = { "com.bigtester.tebloud.machine.fasttext.service.repository",  "com.bigtester.ate.tcg.service.repository"})
@EnableTransactionManagement
@ComponentScan({"com.bigtester.ate.tcg", "com.bigtester.tebloud.machine"})
@PropertySource("classpath:application.properties")

public class Neo4jConfig {

	/** The Constant NEO4JSERVER. use the docker host exposed virtual docker net adapter ip */
	@Value("${neo4jserver_url}")
	public String NEO4JSERVER_URL;// = "http://neo4j-ate-timetree-v-ate-tcg-neo4j-dbdata:7474";


    /**
     * {@inheritDoc}
     * @throws Exception
    */
//    @Bean
//    public Neo4jTemplate neo4jTemplate() throws Exception {//NOPMD
//        return new Neo4jTemplate(getSession());
//    }


    /**
     * Gets the configuration.
     *
     * @return the configuration
     */
    @Bean
    public org.neo4j.ogm.config.Configuration getConfiguration() {
      org.neo4j.ogm.config.Configuration config = new org.neo4j.ogm.config.Configuration();
       config
           .driverConfiguration()
           .setDriverClassName("org.neo4j.ogm.drivers.http.driver.HttpDriver")
           .setURI(NEO4JSERVER_URL);
       return config;
    }
	/**
	 * {@inheritDoc}
	*/
	//@Override
    @Bean
	public SessionFactory sessionFactory() {
		return new SessionFactory(getConfiguration(), "com.bigtester.tebloud.machine.fasttext.model.domain", "com.bigtester.tebloud.machine.fasttext.model.relationship", "com.bigtester.ate.tcg.model.domain", "com.bigtester.ate.tcg.model.relationship");
	}
    @Bean
    public Neo4jTransactionManager transactionManager() {
        return new Neo4jTransactionManager(sessionFactory());
    }
//	/**
//	 * {@inheritDoc}
//	*/
//	@Override
//	public Neo4jServer neo4jServer() {
//		return new RemoteServer(NEO4JSERVER_URL);
//	}
//
	/**
	 * {@inheritDoc}
	*/
//	@Bean
//    //@Scope(value = "session", proxyMode = ScopedProxyMode.TARGET_CLASS)
//    public @Nullable Session getSession() throws Exception {//NOPMD
//        return super.getSession();
//    }
//	 @Bean
//	    public Session getSession(SessionFactory sessionFactory) throws Exception {
//	        SpringSessionProxyBean proxy = new SpringSessionProxyBean();
//	        proxy.setSessionFactory(sessionFactory);
//	        proxy.afterPropertiesSet();
//	        return proxy.getObject();
//	    }
}
