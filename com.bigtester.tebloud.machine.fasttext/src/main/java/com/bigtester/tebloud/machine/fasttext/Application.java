/*******************************************************************************
 * ATE, Automation Test Engine
 *
 * Copyright 2016, Montreal PROT, or individual contributors as
 * indicated by the @author tags or express copyright attribution
 * statements applied by the authors.  All third-party contributions are
 * distributed under license by Montreal PROT.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package com.bigtester.tebloud.machine.fasttext;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.data.neo4j.Neo4jDataAutoConfiguration;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.neo4j.repository.config.EnableNeo4jRepositories;

import com.bigtester.tebloud.machine.fasttext.config.FastTextProperties;

//@SpringBootApplication
@EnableNeo4jRepositories(basePackages = {"com.bigtester.ate.tcg.service.repository","com.bigtester.tebloud.machine.service.repository"})
@SpringBootApplication(exclude={Neo4jDataAutoConfiguration.class})
@ComponentScan({"com.bigtester.ate.tcg", "com.bigtester.tebloud.machine"})
//@EnableAutoConfiguration
public class Application {

	public static void main(String[] args) {
		SpringApplication.run(
				Application.class, args);
	}

}